"""
Homework4.
Replace 'pass' by your implementation.
"""

# Insert your package here
import numpy as np
from helper import refineF
from scipy.interpolate import RectBivariateSpline
import sympy
import scipy

'''
Q2.1: Eight Point Algorithm
    Input:  pts1, Nx2 Matrix
            pts2, Nx2 Matrix
            M, a scalar parameter computed as max (imwidth, imheight)
    Output: F, the fundamental matrix
'''
def eightpoint(pts1, pts2, M):
    
    norm_pts1 = pts1 / M
    norm_pts2 = pts2 / M
    norm_pts1_x = norm_pts1[:, 0]
    norm_pts1_y = norm_pts1[:, 1]
    norm_pts2_x = norm_pts2[:, 0]
    norm_pts2_y = norm_pts2[:, 1]

    A = np.transpose(np.array([norm_pts1_x*norm_pts2_x, norm_pts1_y*norm_pts2_x, norm_pts2_x, norm_pts1_x*norm_pts2_y, norm_pts1_y*norm_pts2_y, norm_pts2_y, norm_pts1_x, norm_pts1_y, np.ones((pts1.shape[0]),
                              dtype=np.float32)]))
    _, _, V = np.linalg.svd(A)
    F = np.reshape(V[-1], (3, 3))
    U2, S2, V2 = np.linalg.svd(F)
    S2_mat = np.diag(S2)
    S2_mat[2, 2] = 0.0
    F = U2 @ S2_mat @ V2
    F = refineF(F, norm_pts1, norm_pts2)

    T = np.array([[1.0/M, 0.0, 0.0], [0.0, 1.0/M, 0.0], [0.0, 0.0, 1.0]], dtype=np.float32)
    F = np.transpose(T) @ F @ T

    np.savez("./q2_1.npz", F=F, M=M)

    return F

'''
Q2.2: Seven Point Algorithm
    Input:  pts1, Nx2 Matrix
            pts2, Nx2 Matrix
            M, a scalar parameter computed as max (imwidth, imheight)
    Output: Farray, a list of estimated fundamental matrix.
'''
def sevenpoint(pts1, pts2, M):
    
    norm_pts1 = pts1 / M
    norm_pts2 = pts2 / M
    norm_pts1_x = norm_pts1[:, 0]
    norm_pts1_y = norm_pts1[:, 1]
    norm_pts2_x = norm_pts2[:, 0]
    norm_pts2_y = norm_pts2[:, 1]

    A = np.transpose(np.array([norm_pts1_x*norm_pts2_x, norm_pts1_y*norm_pts2_x, norm_pts2_x, norm_pts1_x*norm_pts2_y, norm_pts1_y*norm_pts2_y, norm_pts2_y, norm_pts1_x, norm_pts1_y, np.ones((pts1.shape[0]),
                              dtype=np.float32)]))
    _, _, V = np.linalg.svd(A)
    F1 = np.reshape(V[-1], (3, 3))
    F2 = np.reshape(V[-2], (3, 3))

    a = sympy.Symbol('a')
    eq = (a * sympy.Matrix(F1) + (1-a) * sympy.Matrix(F2)).det()
    alpha_list = sympy.solveset(eq, a, domain=sympy.S.Reals)

    Farray = []
    T = np.array([[1.0/M, 0.0, 0.0], [0.0, 1.0/M, 0.0], [0.0, 0.0, 1.0]], dtype=np.float32)
    for alpha in alpha_list:

        alpha = float(alpha)
        F = alpha * F1 + (1 - alpha) * F2
        F = refineF(F, norm_pts1, norm_pts2)
        F = np.transpose(T) @ F @ T
        Farray.append(F)
    
    np.savez("./q2_2.npz", F=Farray[0], M=M, pts1=pts1, pts2=pts2)

    return Farray

'''
Q3.1: Compute the essential matrix E.
    Input:  F, fundamental matrix
            K1, internal camera calibration matrix of camera 1
            K2, internal camera calibration matrix of camera 2
    Output: E, the essential matrix
'''
def essentialMatrix(F, K1, K2):
    
    E = np.transpose(K2) @ F @ K1

    return E

'''
Q3.2: Triangulate a set of 2D coordinates in the image to a set of 3D points.
    Input:  C1, the 3x4 camera matrix
            pts1, the Nx2 matrix with the 2D image coordinates per row
            C2, the 3x4 camera matrix
            pts2, the Nx2 matrix with the 2D image coordinates per row
    Output: P, the Nx3 matrix with the corresponding 3D points per row
            err, the reprojection error.
'''
def triangulate(C1, pts1, C2, pts2):

    num_points = pts1.shape[0]

    P = np.empty((num_points, 3), dtype=np.float32)
    
    for i in range(num_points):

        pt1 = pts1[i]
        pt2 = pts2[i]
    
        A = np.array([(pt1[0] * C1[2, :] - C1[0, :]),
                      (pt1[1] * C1[2, :] - C1[1, :]),
                      (pt2[0] * C2[2, :] - C2[0, :]),
                      (pt2[1] * C2[2, :] - C2[1, :])], dtype=np.float32)

        _, _, V = np.linalg.svd(A)
        homo_point = V[-1]
        homo_point = homo_point / homo_point[-1]
        P[i, :] = homo_point[0:3]

    P_homo = np.append(P, np.ones((num_points, 1)), axis=1)
    p_reproj1 = C1 @ np.transpose(P_homo)
    p_reproj1 = p_reproj1[0:2, :] / p_reproj1[-1, :]
    p_reproj2 = C2 @ np.transpose(P_homo)
    p_reproj2 = p_reproj2[0:2, :] / p_reproj2[-1, :]
    err = np.sum(np.linalg.norm(np.transpose(p_reproj1) - pts1, axis=1) + np.linalg.norm(np.transpose(p_reproj2) - pts2, axis=1))

    return P, err

'''
Q4.1: 3D visualization of the temple images.
    Input:  im1, the first image
            im2, the second image
            F, the fundamental matrix
            x1, x-coordinates of a pixel on im1
            y1, y-coordinates of a pixel on im1
    Output: x2, x-coordinates of the pixel on im2
            y2, y-coordinates of the pixel on im2

'''
def epipolarCorrespondence(im1, im2, F, x1, y1):

    def gauss2D(shape=(3,3), sigma=0.5):

        m, n = [(ss - 1.0) / 2.0 for ss in shape]
        y, x = np.ogrid[-m:m+1, -n:n+1]
        h = np.exp( -(x**2 + y**2) / (2.0*sigma**2) )
        h[ h < np.finfo(h.dtype).eps * h.max() ] = 0
        sumh = h.sum()

        if sumh != 0:
            h /= sumh

        return h

    homo_pt1 = np.array([x1, y1, 1.0], dtype=np.float32)
    ep_line = F @ homo_pt1
    ep_line = ep_line / np.sqrt(ep_line[0]**2+ep_line[1]**2)

    WINDOW_SIZE = 7
    SIGMA = 3
    DISTANCE_THRESH = 400

    win1_idx_x1 = round(x1)-WINDOW_SIZE
    win1_idx_x2 = round(x1)+WINDOW_SIZE+1
    win1_idx_y1 = round(y1)-WINDOW_SIZE
    win1_idx_y2 = round(y1)+WINDOW_SIZE+1

    img1_window = im1[win1_idx_y1:win1_idx_y2, win1_idx_x1:win1_idx_x2, :]

    gauss_window = np.expand_dims(gauss2D((2*WINDOW_SIZE+1, 2*WINDOW_SIZE+1), SIGMA), axis=-1)

    best_diff = np.inf
    best_idx = None
    if not ep_line[0] == 0:
        for y_idx in range(WINDOW_SIZE, im2.shape[0]-WINDOW_SIZE):

            x_idx = -(ep_line[1] * y_idx + ep_line[2]) / ep_line[0]

            if ((y_idx - y1) ** 2 + (x_idx - x1) ** 2) > DISTANCE_THRESH: continue

            if x_idx > WINDOW_SIZE and x_idx < im2.shape[1]-WINDOW_SIZE:

                win2_idx_x1 = int(round(x_idx-WINDOW_SIZE))
                win2_idx_x2 = int(round(x_idx+WINDOW_SIZE+1))
                win2_idx_y1 = y_idx-WINDOW_SIZE
                win2_idx_y2 = y_idx+WINDOW_SIZE+1

                img2_window = im2[win2_idx_y1:win2_idx_y2, win2_idx_x1:win2_idx_x2, :]
                diff = np.linalg.norm(gauss_window * (img2_window - img1_window))

                if diff < best_diff:
                    best_diff = diff
                    best_idx = [y_idx, x_idx]
    else:
        for x_idx in range(WINDOW_SIZE, im2.shape[1]-WINDOW_SIZE):

            y_idx = -(ep_line[0] * x_idx + ep_line[2]) / ep_line[1]

            if ((y_idx - y1) ** 2 + (x_idx - x1) ** 2) > DISTANCE_THRESH: continue

            if x_idx > WINDOW_SIZE and x_idx < im2.shape[1]-WINDOW_SIZE:

                win2_idx_x1 = x_idx-WINDOW_SIZE
                win2_idx_x2 = x_idx+WINDOW_SIZE+1
                win2_idx_y1 = int(round(y_idx-WINDOW_SIZE))
                win2_idx_y2 = int(round(y_idx+WINDOW_SIZE+1))

                img2_window = im2[win2_idx_y1:win2_idx_y2, win2_idx_x1:win2_idx_x2, :]
                diff = np.linalg.norm(gauss_window * (img2_window - img1_window))

                if diff < best_diff:
                    best_diff = diff
                    best_idx = [y_idx, x_idx]

    y2 = best_idx[0]
    x2 = best_idx[1]

    return x2, y2

'''
Q5.1: RANSAC method.
    Input:  pts1, Nx2 Matrix
            pts2, Nx2 Matrix
            M, a scaler parameter
    Output: F, the fundamental matrix
            inliers, Nx1 bool vector set to true for inliers
'''
def ransacF(pts1, pts2, M):

    MAX_ITERATION = 150
    TOLERANCE = 2

    num_points = pts1.shape[0]
    
    highest_inlier_count = 0
    best_inlier_idxes = None
    inliers = None
    for j in range(MAX_ITERATION):

        rand_idxes = np.random.randint(0, pts1.shape[0], 8)
        rand_pts1 = np.take(pts1, rand_idxes, axis=0)
        rand_pts2 = np.take(pts2, rand_idxes, axis=0)

        Farray = sevenpoint(rand_pts1, rand_pts2, M)

        for F_cand in Farray:

            dist = []

            for i in range(num_points):
                homo_pt1 = np.array([pts1[i, 0], pts1[i, 1], 1.0], dtype=np.float32)
                ep_line = F_cand @ homo_pt1
                ep_line = ep_line / np.sqrt(ep_line[0]**2+ep_line[1]**2)
                dist.append(ep_line[0]*pts2[i, 0] + ep_line[1]*pts2[i, 1] + ep_line[2])
            
            dist = np.absolute(np.stack(dist, axis=0))
            inlier_bool = np.less(dist, TOLERANCE)
            inlier_idxes = np.argwhere(inlier_bool.astype(np.int)==1)
            inlier_count = np.sum(inlier_bool.astype(np.int))
            
            if inlier_count > highest_inlier_count:
                highest_inlier_count = inlier_count
                inliers = inlier_bool
                best_inlier_idxes = np.ndarray.flatten(inlier_idxes)
    
    fit_pts1 = np.take(pts1, best_inlier_idxes, axis=0)
    fit_pts2 = np.take(pts2, best_inlier_idxes, axis=0)
    F = eightpoint(fit_pts1, fit_pts2, M)

    return F, inliers

'''
Q5.2: Rodrigues formula.
    Input:  r, a 3x1 vector
    Output: R, a rotation matrix
'''
def rodrigues(r):

    axis = r / np.linalg.norm(r)
    theta = np.linalg.norm(r)
    
    K = np.array([[0, -axis[2], axis[1]],
                  [axis[2], 0, -axis[0]],
                  [-axis[1], axis[0], 0]], dtype=np.float32)
    I = np.identity(3)

    R = I + np.sin(theta) * K + (1 - np.cos(theta)) * (K @ K)

    return R

'''
Q5.2: Inverse Rodrigues formula.
    Input:  R, a rotation matrix
    Output: r, a 3x1 vector
'''
def invRodrigues(R):
    
    # Implementation of https://www2.cs.duke.edu/courses/compsci527/fall13/notes/rodrigues.pdf

    I = np.identity(3)

    A = 0.5 * (R - R.T)
    p = np.array([[A[2][1]], [A[0][2]], [A[1][0]]], dtype=np.float32)
    s = np.linalg.norm(p)
    c = 0.5 * (R[0][0] + R[1][1] + R[2][2] - 1.0)

    if s == 0 and c == 1:
        r = np.zeros((3, ), dtype=np.float32)
    elif s == 0 and c == -1:
        for col in np.transpose(R+I):
            if not col == np.zeros((3, ), dtype=np.float32):
                v = col
        u = v / np.linalg.norm(v)
        r = u * np.pi
        if np.linalg.norm(r) == np.pi and ( (r[0] == 0 and r[1] == 0 and r[2] < 0) or (r[0] == 0 and r[1] < 0) or (r[0] < 0) ):
            r = -r
    elif not s == 0:
        u = p / s
        theta = np.arctan2(s, c)
        r = theta * u
    else:
        print ("WHAT")
    
    return r

'''
Q5.3: Rodrigues residual.
    Input:  K1, the intrinsics of camera 1
            M1, the extrinsics of camera 1
            p1, the 2D coordinates of points in image 1
            K2, the intrinsics of camera 2
            p2, the 2D coordinates of points in image 2
            x, the flattened concatenationg of P, r2, and t2.
    Output: residuals, 4N x 1 vector, the difference between original and estimated projections
'''
def rodriguesResidual(K1, M1, p1, K2, p2, x):

    num_points = p1.shape[0]
    P, r2, t2 = np.split(x, [-6, -3], axis=0)
    P = np.reshape(P, [-1, 3])
    R = rodrigues(r2)
    T = np.expand_dims(t2, axis=1)
    M2 = np.append(R, T, axis=1)
    C1 = K1 @ M1
    C2 = K2 @ M2

    P_homo = np.append(P, np.ones((num_points, 1)), axis=1)
    p_reproj1 = C1 @ np.transpose(P_homo)
    p_reproj1 = p_reproj1[0:2, :] / p_reproj1[-1, :]
    p_reproj2 = C2 @ np.transpose(P_homo)
    p_reproj2 = p_reproj2[0:2, :] / p_reproj2[-1, :]
    residuals = np.concatenate([np.reshape(np.transpose(p_reproj1) - p1, [-1]), np.reshape(np.transpose(p_reproj2) - p2, [-1])])
    residuals = np.expand_dims(residuals, axis=-1)

    return residuals

'''
Q5.3 Bundle adjustment.
    Input:  K1, the intrinsics of camera 1
            M1, the extrinsics of camera 1
            p1, the 2D coordinates of points in image 1
            K2,  the intrinsics of camera 2
            M2_init, the initial extrinsics of camera 2
            p2, the 2D coordinates of points in image 2
            P_init, the initial 3D coordinates of points
    Output: M2, the optimized extrinsics of camera 2
            P2, the optimized 3D coordinates of points
'''
def bundleAdjustment(K1, M1, p1, K2, M2_init, p2, P_init):
    
    R2, T2 = np.split(M2_init, [-1], axis=1)
    r2 = np.ndarray.flatten(invRodrigues(R2))
    t2 = np.ndarray.flatten(T2)
    P_init_flattened = np.ndarray.flatten(P_init)
    x_init = np.concatenate([P_init_flattened, r2, t2])
    res_function = lambda x: np.squeeze(rodriguesResidual(K1, M1, p1, K2, p2, x), axis=-1)
    x = scipy.optimize.leastsq(res_function, x_init)[0]

    P2, r2, t2 = np.split(x, [-6, -3], axis=0)
    P2 = np.reshape(P2, [-1, 3])
    R = rodrigues(r2)
    T = np.expand_dims(t2, axis=1)
    M2 = np.append(R, T, axis=1)

    return M2, P2