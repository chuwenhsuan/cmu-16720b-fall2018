'''
Q3.3:
    1. Load point correspondences
    2. Obtain the correct M2
    3. Save the correct M2, C2, p1, p2, R and P to q3_3.mat
'''

import submission
import helper
import numpy as np
import cv2

if __name__ == '__main__':

    img1 = cv2.cvtColor(cv2.imread("../data/im1.png"), cv2.COLOR_BGR2RGB)
    img2 = cv2.cvtColor(cv2.imread("../data/im2.png"), cv2.COLOR_BGR2RGB)
    assert img1.shape == img2.shape
    dict_corresp = np.load("../data/some_corresp.npz")
    dict_intrinsics = np.load("../data/intrinsics.npz")
    pts1 = dict_corresp["pts1"]
    pts2 = dict_corresp["pts2"]
    K1 = dict_intrinsics["K1"]
    K2 = dict_intrinsics["K2"]
    max_dim = np.amax(img1.shape)
    F = submission.eightpoint(pts1, pts2, M=max_dim)
    E = submission.essentialMatrix(F, K1, K2)
    M1 = np.append(np.identity(3), np.array([[0.0], [0.0], [0.0]]), axis=1)
    C1 = K1 @ M1
    M2_list = helper.camera2(E)
    
    M2_best = None
    C2_best = None
    P_best = None
    least_neg_z = np.inf
    error_best = np.inf
    for i in range(M2_list.shape[-1]):

        M2 = M2_list[:, :, i]
        C2 = K2 @ M2
        P, error = submission.triangulate(C1, pts1, C2, pts2)
        neg_z_count = np.sum(np.less(P[:, 2], 0.0).astype(int))

        if neg_z_count < least_neg_z:

                M2_best = M2
                C2_best = C2
                P_best = P
                error_best = error
                least_neg_z = neg_z_count
      
        elif neg_z_count == least_neg_z:
            if error < error_best:

                M2_best = M2
                C2_best = C2
                P_best = P
                error_best = error
                least_neg_z = neg_z_count

    np.savez("./q3_3.npz", M2=M2_best, C2=C2_best, P=P_best)