import submission
import helper
import numpy as np
import cv2

if __name__ == '__main__':

    img1 = cv2.cvtColor(cv2.imread("../data/im1.png"), cv2.COLOR_BGR2RGB)
    img2 = cv2.cvtColor(cv2.imread("../data/im2.png"), cv2.COLOR_BGR2RGB)
    assert img1.shape == img2.shape
    dict_corresp = np.load("../data/some_corresp.npz")
    dict_noise_corresp = np.load("../data/some_corresp_noisy.npz")
    dict_intrinsics = np.load("../data/intrinsics.npz")
    pts1 = dict_corresp["pts1"]
    pts2 = dict_corresp["pts2"]
    noisy_pts1 = dict_noise_corresp["pts1"]
    noisy_pts2 = dict_noise_corresp["pts2"]
    K1 = dict_intrinsics["K1"]
    K2 = dict_intrinsics["K2"]
    max_dim = np.amax(img1.shape)
    #F, inliers = submission.ransacF(noisy_pts1, noisy_pts2, max_dim)
    #F = submission.eightpoint(noisy_pts1, noisy_pts2, M=max_dim)
    F = submission.eightpoint(pts1, pts2, M=max_dim)
    #E = submission.essentialMatrix(F, K1, K2)
    #helper.epipolarMatchGUI(img1, img2, F)
    #print (F)
    #print (E)
    helper.displayEpipolarF(img1, img2, F)
    #indices = np.random.randint(0, pts1.shape[0], 7)
    #Farray = submission.sevenpoint(np.take(pts1, indices, axis=0), np.take(pts2, indices, axis=0), M=max_dim)
    #F, inliers = submission.ransacF(pts1, pts2, max_dim)
    #print (Farray[0])
    #helper.displayEpipolarF(img1, img2, Farray[0])