'''
Q4.2:
    1. Integrating everything together.
    2. Loads necessary files from ../data/ and visualizes 3D reconstruction using scatter3
'''

import submission
import helper
import numpy as np
import cv2
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

if __name__ == '__main__':

    img1 = cv2.cvtColor(cv2.imread("../data/im1.png"), cv2.COLOR_BGR2RGB)
    img2 = cv2.cvtColor(cv2.imread("../data/im2.png"), cv2.COLOR_BGR2RGB)
    assert img1.shape == img2.shape
    dict_corresp = np.load("../data/some_corresp.npz")
    dict_intrinsics = np.load("../data/intrinsics.npz")
    dict_temple = np.load('../data/templeCoords.npz')
    pts1 = dict_corresp["pts1"]
    pts2 = dict_corresp["pts2"]
    K1 = dict_intrinsics["K1"]
    K2 = dict_intrinsics["K2"]
    x1 = dict_temple["x1"]
    y1 = dict_temple["y1"]

    max_dim = np.amax(img1.shape)
    F = submission.eightpoint(pts1, pts2, M=max_dim)
    x2 = []
    y2 = []
    for x, y in zip(x1, y1):
        x_match, y_match = submission.epipolarCorrespondence(img1, img2, F, x[0], y[0])
        x2.append(x_match)
        y2.append(y_match)
    x2 = np.stack(x2, axis=0)
    y2 = np.stack(y2, axis=0)
    pts1 = np.squeeze(np.stack([x1, y1], axis=1), axis=-1)
    pts2 = np.stack([x2, y2], axis=1)

    E = submission.essentialMatrix(F, K1, K2)
    M1 = np.append(np.identity(3), np.array([[0.0], [0.0], [0.0]]), axis=1)
    C1 = K1 @ M1
    M2_list = helper.camera2(E)
    M2_best = None
    C2_best = None
    P_best = None
    least_neg_z = np.inf
    error_best = np.inf
    for i in range(M2_list.shape[-1]):

        M2 = M2_list[:, :, i]
        C2 = K2 @ M2
        P, error = submission.triangulate(C1, pts1, C2, pts2)
        neg_z_count = np.sum(np.less(P[:, 2], 0.0).astype(int))

        if neg_z_count < least_neg_z:

                M2_best = M2
                C2_best = C2
                P_best = P
                error_best = error
                least_neg_z = neg_z_count
      
        elif neg_z_count == least_neg_z:
            if error < error_best:

                M2_best = M2
                C2_best = C2
                P_best = P
                error_best = error
                least_neg_z = neg_z_count

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(P_best[:, 0], P_best[:, 1], P_best[:, 2], c='b', marker='.')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    np.savez("./q4_2.npz", F=F, M1=M1, M2=M2_best, C1=C1, C2=C2_best)

    plt.show()