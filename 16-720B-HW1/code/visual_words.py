import numpy as np
import multiprocessing
import imageio
import scipy.ndimage
import skimage.color
import sklearn.cluster
import scipy.spatial.distance
import os,time
import matplotlib.pyplot as plt
import util
import random

def extract_filter_responses(image):
	'''
	Extracts the filter responses for the given image.

	[input]
	* image: numpy.ndarray of shape (H,W) or (H,W,3)
	[output]
	* filter_responses: numpy.ndarray of shape (H,W,3F)
	'''

	SCALES = [1.0, 2.0, 4.0, 8.0, 11.3137]

	image = image.astype(np.float32)

	img_shape = image.shape

	if np.amax(image) > 1.0: image /= 255.0
	if len(img_shape) == 2: image = np.stack([image, image, image], axis=2)
	if img_shape[2] > 3: image = image[:, :, 0:3]

	image = skimage.color.rgb2lab(image)

	filter_responses = []

	for scale in SCALES:

		for ch in range(3):
			filter_responses.append(scipy.ndimage.gaussian_filter(image[:, :, ch], scale, order=0))
		for ch in range(3):
			filter_responses.append(scipy.ndimage.gaussian_laplace(image[:, :, ch], scale))
		for ch in range(3):
			filter_responses.append(scipy.ndimage.gaussian_filter(image[:, :, ch], scale, order=(0, 1)))
		for ch in range(3):
			filter_responses.append(scipy.ndimage.gaussian_filter(image[:, :, ch], scale, order=(1, 0)))

	filter_responses = np.stack(filter_responses, axis=2)

	return filter_responses

def get_visual_words(image,dictionary):
	'''
	Compute visual words mapping for the given image using the dictionary of visual words.

	[input]
	* image: numpy.ndarray of shape (H,W) or (H,W,3)
	
	[output]
	* wordmap: numpy.ndarray of shape (H,W)
	'''

	filter_responses = extract_filter_responses(image)
	response_shape = filter_responses.shape
	filter_responses = np.reshape(filter_responses, [-1, response_shape[-1]])
	mat_dist = scipy.spatial.distance.cdist(filter_responses, dictionary)
	wordmap = np.argmin(mat_dist, axis=1).reshape(image.shape[0:2])

	return wordmap

def compute_dictionary_one_image(args):
	'''
	Extracts random samples of the dictionary entries from an image.
	This is a function run by a subprocess.

	[input]
	* i: index of training image
	* alpha: number of random samples
	* image_path: path of image file
	* time_start: time stamp of start time

	[saved]
	* sampled_response: numpy.ndarray of shape (alpha,3F)
	'''

	i, alpha, image_path, _ = args
	
	img = skimage.io.imread(image_path)
	filter_responses = extract_filter_responses(img)
	response_shape = filter_responses.shape
	filter_responses = np.reshape(filter_responses, [-1, response_shape[-1]])
	sampled_responses = np.random.permutation(filter_responses)[0:alpha]

	np.savez("./temp/temp_response_{}".format(str(i)), sampled_responses)

def compute_dictionary(num_workers=2):
	'''
	Creates the dictionary of visual words by clustering using k-means.

	[input]
	* num_workers: number of workers to process in parallel
	
	[saved]
	* dictionary: numpy.ndarray of shape (K,3F)
	'''

	NUM_CLUSTERS = 150
	ALPHA = 200

	if not os.path.isdir("./temp/"): os.mkdir("./temp/")
	train_data = np.load("../data/train_data.npz")

	img_paths = train_data["image_names"]
	img_count = len(img_paths)

	processes = []

	for i in range(0, img_count, num_workers):

		for j in range(num_workers):
			
			if (i + j) < img_count:

				img_path = "../data/{}".format(img_paths[i+j][0])
				function_args = (i+j, ALPHA, img_path, time.ctime())
				p = multiprocessing.Process(target=compute_dictionary_one_image, args=(function_args, ) )
				processes.append(p)
				p.start()
    
		for process in processes:

			process.join()
		
		processes.clear()
	
	responses = []

	for i in range(img_count):

		response = np.load("./temp/temp_response_{}.npz".format(str(i)))["arr_0"]
		responses.append(response)

	responses = np.concatenate(responses, axis=0)
	kmeans = sklearn.cluster.KMeans(n_clusters=NUM_CLUSTERS, n_jobs=num_workers).fit(responses)
	dictionary = kmeans.cluster_centers_
	np.save("dictionary", dictionary)