import numpy as np
import multiprocessing
import threading
import queue
import imageio
import os,time
import torch
import skimage.transform
import torchvision.transforms
import util
import network_layers

def build_recognition_system(vgg16,num_workers=2):
	'''
	Creates a trained recognition system by generating training features from all training images.

	[input]
	* vgg16: prebuilt VGG-16 network.
	* num_workers: number of workers to process in parallel

	[saved]
	* features: numpy.ndarray of shape (N,K)
	* labels: numpy.ndarray of shape (N)
	'''

	if not os.path.isdir("./temp/"): os.mkdir("./temp/")
	train_data = np.load("../data/train_data.npz")
	
	img_paths = train_data["image_names"]
	labels = train_data["labels"]

	img_count = len(img_paths)
	
	processes = []

	for i in range(0, img_count, num_workers):

		for j in range(num_workers):
			
			if (i + j) < img_count:

				img_path = "../data/{}".format(img_paths[i][0])
				args = (i+j, img_path, vgg16, time.ctime())
				p = multiprocessing.Process(target=get_image_feature, args=(args, ) )
				p.start()
				processes.append(p)
    
		for j in range(len(processes)):

			processes[j].join()
		
		processes.clear()
	
	features = []

	for i in range(img_count):

		feature = np.load("./temp/temp_response_deep{}.npz".format(str(i)))["arr_0"]
		features.append(feature)

	features = np.concatenate(features, axis=0)

	np.savez("./trained_system_deep", features=features, labels=labels)

def evaluate_recognition_system(vgg16,num_workers=2):
	'''
	Evaluates the recognition system for all test images and returns the confusion matrix.

	[input]
	* vgg16: prebuilt VGG-16 network.
	* num_workers: number of workers to process in parallel

	[output]
	* conf: numpy.ndarray of shape (8,8)
	* accuracy: accuracy of the evaluated system
	'''
	
	test_data = np.load("../data/test_data.npz")
	trained_system = np.load("trained_system_deep.npz")

	img_paths = test_data["image_names"]
	test_labels = test_data["labels"]

	trained_features = trained_system["features"]
	trained_labels = trained_system["labels"]

	img_count = len(img_paths)

	conf = np.zeros((8, 8))
	num_correct = 0
	
	features = []
	threads = []
	feature_queue = queue.Queue()
	for i in range(0, img_count, num_workers):

		for j in range(num_workers):

			if (i + j) < img_count:

				img_path = "../data/{}".format(img_paths[i][0])
				args = (feature_queue, img_path, vgg16)
				calc_feature_thread = threading.Thread(target=put_image_feature_queue, args=args)
				threads.append(calc_feature_thread)
				calc_feature_thread.start()

		for thread in threads:

			thread.join()

		while not feature_queue.empty():

			feature = feature_queue.get()
			features.append(feature)

		threads.clear()
	
	features = np.stack(features, axis=0)
	
	for idx, img_feature in enumerate(features):

		sim = distance_to_set(img_feature, trained_features)
		pred_label = trained_labels[np.argmax(sim)]
		true_label = test_labels[idx]

		if pred_label == true_label: num_correct += 1
		
		conf[true_label][pred_label] += 1
	
	accuracy = num_correct / img_count

	return conf, accuracy

def preprocess_image(image):
	'''
	Preprocesses the image to load into the prebuilt network.

	[input]
	* image: numpy.ndarray of shape (H,W,3)

	[output]
	* image_processed: torch.Tensor of shape (3,H,W)
	'''

	MEAN_CH = np.array([0.485, 0.456, 0.406])
	STD_CH = np.array([0.229, 0.224, 0.225])

	resized_img = skimage.transform.resize(image, (224, 224, 3), mode="constant", anti_aliasing=True)
	normed_img = (resized_img - MEAN_CH) / STD_CH
	image_processed = np.transpose(normed_img, [2, 0, 1]).astype(np.float32)

	return image_processed

def put_image_feature_queue(queue, image_path, vgg16):

	img = preprocess_image(imageio.imread(image_path))
	feat = torch.autograd.Variable(torch.from_numpy(np.expand_dims(img, axis=0))).double()

	conv_layer_list = list(vgg16.features)
	fc_layer_list = list(vgg16.classifier)[:-3]
	for layer in conv_layer_list:
		feat = layer(feat)
	feat = feat.view(feat.size(0), -1)
	for layer in fc_layer_list:
		feat = layer(feat)

	queue.put(feat.detach().cpu().numpy())

def get_image_feature(args):
	'''
	Extracts deep features from the prebuilt VGG-16 network.
	This is a function run by a subprocess.
 	[input]
	* i: index of training image
	* image_path: path of image file
	* vgg16: prebuilt VGG-16 network.
	* time_start: time stamp of start time
 	[saved]
	* feat: evaluated deep feature
	'''

	i, image_path, vgg16, _ = args

	img = preprocess_image(imageio.imread(image_path))
	feat = torch.autograd.Variable(torch.from_numpy(np.expand_dims(img, axis=0))).double()

	conv_layer_list = list(vgg16.features)
	fc_layer_list = list(vgg16.classifier)[:-3]
	for layer in conv_layer_list:
		feat = layer(feat)
	feat = feat.view(feat.size(0), -1)
	for layer in fc_layer_list:
		feat = layer(feat)

	np.savez("./temp/temp_response_deep{}".format(str(i)), feat.detach().numpy())

def distance_to_set(feature,train_features):
	'''
	Compute distance between a deep feature with all training image deep features.

	[input]
	* feature: numpy.ndarray of shape (K)
	* train_features: numpy.ndarray of shape (N,K)

	[output]
	* dist: numpy.ndarray of shape (N)
	'''

	dist = 0.0 - np.linalg.norm(feature-train_features, axis=1)

	return dist