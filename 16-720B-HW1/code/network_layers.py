import numpy as np
import scipy.ndimage
import os,time

def extract_deep_feature(x,vgg16_weights):
	
	'''
	Extracts deep features from the given VGG-16 weights.

	[input]
	* x: numpy.ndarray of shape (H,W,3)
	* vgg16_weights: numpy.ndarray of shape (L,3)

	[output]
	* feat: numpy.ndarray of shape (K)
	'''

	feat = x

	for idx, layer_info in enumerate(vgg16_weights):

		if idx < len(vgg16_weights)-3:
			
			if layer_info[0] == "conv2d":
				feat = multichannel_conv2d(feat, layer_info[1], layer_info[2])

			elif layer_info[0] == "relu":
				feat = relu(feat)
			
			elif layer_info[0] == "maxpool2d":
				feat = max_pool2d(feat, layer_info[1])
			
			elif layer_info[0] == "linear":
				if len(feat.shape) == 3:
					feat = np.transpose(feat, [2, 0, 1])
				feat = np.reshape(feat, [-1])
				feat = linear(feat, layer_info[1], layer_info[2])
			
			else:
				print ("Unknown layer type: {}!".format(layer_info[0])) # DEBUG only!
				pass

	return feat

def multichannel_conv2d(x,weight,bias):
	'''
	Performs multi-channel 2D convolution.

	[input]
	* x: numpy.ndarray of shape (H,W,input_dim)
	* weight: numpy.ndarray of shape (output_dim,input_dim,kernel_size,kernel_size)
	* bias: numpy.ndarray of shape (output_dim)

	[output]
	* feat: numpy.ndarray of shape (H,W,output_dim)
	'''

	feat = []

	x_reshaped = np.transpose(x, [2, 0, 1])

	for i, kernel in enumerate(weight):

		feat_ch = np.zeros([x_reshaped.shape[1], x_reshaped.shape[2]])
		for j, x_ch in enumerate(x_reshaped):
			kernel_flipped = np.fliplr(np.flipud(kernel[j]))
			feat_ch += scipy.ndimage.convolve(x_ch, kernel_flipped, mode="constant")

		feat.append(feat_ch)

	feat = np.stack(feat, axis=2)
	feat += bias

	return feat

def relu(x):
	'''
	Rectified linear unit.

	[input]
	* x: numpy.ndarray

	[output]
	* y: numpy.ndarray
	'''

	y = np.maximum(0.0, x)
	return y

def max_pool2d(x,size):
	'''
	2D max pooling operation.

	[input]
	* x: numpy.ndarray of shape (H,W,input_dim)
	* size: pooling receptive field

	[output]
	* y: numpy.ndarray of shape (H/size,W/size,input_dim)
	'''
	
	input_dim = x.shape
	y = x.reshape(input_dim[0]//size, size, input_dim[1]//size, size, input_dim[2]).max(axis=(1,3)).reshape(input_dim[0]//size, input_dim[1]//size, input_dim[2])

	return y

def linear(x,W,b):
	'''
	Fully-connected layer.

	[input]
	* x: numpy.ndarray of shape (input_dim)
	* weight: numpy.ndarray of shape (output_dim,input_dim)
	* bias: numpy.ndarray of shape (output_dim)

	[output]
	* y: numpy.ndarray of shape (output_dim)
	'''
	
	y = W @ x + b
	return y

