import numpy as np
import threading
import queue
import imageio
import os,time
import math
import visual_words

def build_recognition_system(num_workers=2):
	'''
	Creates a trained recognition system by generating training features from all training images.

	[input]
	* num_workers: number of workers to process in parallel

	[saved]
	* features: numpy.ndarray of shape (N,M)
	* labels: numpy.ndarray of shape (N)
	* dictionary: numpy.ndarray of shape (K,3F)
	* SPM_layer_num: number of spatial pyramid layers
	'''

	SPM_layer_num = 3

	train_data = np.load("../data/train_data.npz")
	dictionary = np.load("dictionary.npy")
	
	img_paths = train_data["image_names"]
	labels = train_data["labels"]

	img_count = len(img_paths)
	dict_size = dictionary.shape[0]

	features = []
	threads = []
	feature_queue = queue.Queue()
	for i in range(0, img_count, num_workers):

		for j in range(num_workers):

			if (i + j) < img_count:

				img_path = "../data/{}".format(img_paths[i][0])
				args = (img_path, dictionary, SPM_layer_num, dict_size)
				calc_feature_thread = threading.Thread(target=put_image_feature_queue, args=(feature_queue, args))
				threads.append(calc_feature_thread)
				calc_feature_thread.start()

		for thread in threads:

			thread.join()

		while not feature_queue.empty():

			feature = feature_queue.get()
			features.append(feature)

		threads.clear()

	features = np.stack(features, axis=0)

	np.savez("./trained_system", features=features, labels=labels, dictionary=dictionary, SPM_layer_num=SPM_layer_num)

def evaluate_recognition_system(num_workers=2):
	'''
	Evaluates the recognition system for all test images and returns the confusion matrix.

	[input]
	* num_workers: number of workers to process in parallel

	[output]
	* conf: numpy.ndarray of shape (8,8)
	* accuracy: accuracy of the evaluated system
	'''

	test_data = np.load("../data/test_data.npz")
	trained_system = np.load("trained_system.npz")

	img_paths = test_data["image_names"]
	test_labels = test_data["labels"]

	trained_features = trained_system["features"]
	trained_labels = trained_system["labels"]
	trained_dictionary = trained_system["dictionary"]
	SPM_layer_num = trained_system["SPM_layer_num"]

	img_count = len(img_paths)
	dict_size = trained_dictionary.shape[0]

	conf = np.zeros((8, 8))
	num_correct = 0
	
	features = []
	threads = []
	feature_queue = queue.Queue()
	for i in range(0, img_count, num_workers):

		for j in range(num_workers):

			if (i + j) < img_count:

				img_path = "../data/{}".format(img_paths[i][0])
				args = (img_path, trained_dictionary, SPM_layer_num, dict_size)
				calc_feature_thread = threading.Thread(target=put_image_feature_queue, args=(feature_queue, args))
				threads.append(calc_feature_thread)
				calc_feature_thread.start()

		for thread in threads:

			thread.join()

		while not feature_queue.empty():

			feature = feature_queue.get()
			features.append(feature)

		threads.clear()
	
	features = np.stack(features, axis=0)
	
	for idx, img_feature in enumerate(features):

		sim = distance_to_set(img_feature, trained_features)
		pred_label = trained_labels[np.argmax(sim)]
		true_label = test_labels[idx]

		if pred_label == true_label: num_correct += 1
		
		conf[true_label][pred_label] += 1
	
	accuracy = num_correct / img_count

	return conf, accuracy

def put_image_feature_queue(queue, args):
	'''
	Helper function that passes the image features into a queue.

	[input]
	* queue: queue to place features in
	* args: a tuple containing the arguments for get_image_feature()
	'''
	
	queue.put(get_image_feature(*args))

def get_image_feature(file_path,dictionary,layer_num,K):
	'''
	Extracts the spatial pyramid matching feature.

	[input]
	* file_path: path of image file to read
	* dictionary: numpy.ndarray of shape (K,3F)
	* layer_num: number of spatial pyramid layers
	* K: number of clusters for the word maps

	[output]
	* feature: numpy.ndarray of shape (K)
	'''

	img = imageio.imread(file_path)
	wordmap = visual_words.get_visual_words(img, dictionary)
	feature = get_feature_from_wordmap_SPM(wordmap, layer_num, K)

	return feature

def distance_to_set(word_hist,histograms):
	'''
	Compute similarity between a histogram of visual words with all training image histograms.

	[input]
	* word_hist: numpy.ndarray of shape (K)
	* histograms: numpy.ndarray of shape (N,K)

	[output]
	* sim: numpy.ndarray of shape (N)
	'''
	
	sim = np.sum(np.minimum(histograms, word_hist), axis=1)

	return sim

def get_feature_from_wordmap(wordmap,dict_size):
	'''
	Compute histogram of visual words.

	[input]
	* wordmap: numpy.ndarray of shape (H,W)
	* dict_size: dictionary size K

	[output]
	* hist: numpy.ndarray of shape (K)
	'''
	
	hist = np.histogram(wordmap, bins=dict_size, density=True)[0]

	return hist

def get_feature_from_wordmap_SPM(wordmap,layer_num,dict_size):
	'''
	Compute histogram of visual words using spatial pyramid matching.

	[input]
	* wordmap: numpy.ndarray of shape (H,W)
	* layer_num: number of spatial pyramid layers
	* dict_size: dictionary size K

	[output]
	* hist_all: numpy.ndarray of shape (K*(4^layer_num-1)/3)
	'''

	hist_all = []

	split1d = np.array_split(wordmap, 2**(layer_num-1), axis=0)

	bottommost_cells = []
	for subarray in split1d:
		split2d = np.array_split(subarray, 2**(layer_num-1), axis=1)
		bottommost_cells.extend(split2d)
	weight = 0.5
	
	bottommost_hist = []
	for cell in bottommost_cells:
		cell_hist = get_feature_from_wordmap(cell, dict_size)
		bottommost_hist.append(cell_hist)
		hist_all.extend(weight*cell_hist)
	
	prev_layer_hist = bottommost_hist
	for i in range(layer_num-2, -1, -1): 

		if not i == 0: weight = weight * 0.5

		num_cells_per_row = 2 ** i
		curr_layer_hist = []

		for j in range(num_cells_per_row//2+1):
			for k in range(num_cells_per_row):
				cell = 0.25 * (prev_layer_hist[2*j*num_cells_per_row+k] + prev_layer_hist[2*j*num_cells_per_row+k+1] +
				               prev_layer_hist[2*(j+1)*num_cells_per_row+k] + prev_layer_hist[2*(j+1)*num_cells_per_row+k+1])
				curr_layer_hist.append(cell)
				hist_all.extend(weight*cell)

		prev_layer_hist = curr_layer_hist
	
	hist_all = np.array(hist_all, dtype=np.float32)

	return hist_all