import numpy as np
from scipy.ndimage.interpolation import shift
from skimage.transform import resize
from scipy.spatial.distance import cosine as cosine_dist

def recurseNCC(red, green, blue, W=30):

    red_shape = red.shape
    green_shape = green.shape
    blue_shape = blue.shape

    red_pyramid = [red]
    green_pyramid = [green]
    blue_pyramid = [blue]

    layers = int(np.ceil(np.log2(30)))

    for i in range(layers):

        red_downsampled = resize(red_pyramid[i], (red_shape[0] // (2 ** (i+1)), red_shape[1] // (2 ** (i+1))),
                                 mode="constant", anti_aliasing=True)
        green_downsampled = resize(green_pyramid[i], (green_shape[0] // (2 ** (i+1)), green_shape[1] // (2 ** (i+1))),
                                   mode="constant", anti_aliasing=True)
        blue_downsampled = resize(blue_pyramid[i], (blue_shape[0] // (2 ** (i+1)), blue_shape[1] // (2 ** (i+1))),
                                  mode="constant", anti_aliasing=True)

        red_pyramid.append(red_downsampled)
        green_pyramid.append(green_downsampled)
        blue_pyramid.append(blue_downsampled)
    
    offset_red = (0, 0)
    offset_green = (0, 0)
    
    for i in range(-1, -1-layers, -1):
        
        redshift_optimum, greenshift_optimum = calcNCC(red_pyramid[i], green_pyramid[i], blue_pyramid[i], 2, offset_red, offset_green)
        offset_red = tuple(2*x for x in redshift_optimum)
        offset_green = tuple(2*x for x in greenshift_optimum)

    return offset_red, offset_green

def calcNCC(red, green, blue, W=2, start_offset_red=(0, 0), start_offset_green=(0,0)):

    ncc_table_red = np.zeros(shape=(2*W+1, 2*W+1), dtype=np.float32)
    ncc_table_green = np.zeros(shape=(2*W+1, 2*W+1), dtype=np.float32)

    for i in range(-W, 1+W, 1):
        for j in range(-W, 1+W, 1):
            
            red_shift = shift(red, (i+start_offset_red[0], j+start_offset_red[1]))
            green_shift = shift(green, (i+start_offset_green[0], j+start_offset_green[1]))

            red_shift_vector = np.ndarray.flatten(red_shift)
            green_shift_vector = np.ndarray.flatten(green_shift)
            blue_vector = np.ndarray.flatten(blue)

            ncc_red = 1 - cosine_dist(red_shift_vector, blue_vector)
            ncc_green = 1- cosine_dist(green_shift_vector, blue_vector)

            ncc_table_red[i+W][j+W] = ncc_red
            ncc_table_green[i+W][j+W] = ncc_green
    
    red_shift_idx = np.unravel_index(np.argmax(ncc_table_red), ncc_table_red.shape)
    red_shift_optimum = (red_shift_idx[0] - W + start_offset_red[0], red_shift_idx[1] - W + start_offset_red[1])
    green_shfit_idx = np.unravel_index(np.argmax(ncc_table_green), ncc_table_green.shape)
    green_shift_optimum = (green_shfit_idx[0] - W + start_offset_green[0], green_shfit_idx[1] - W + start_offset_green[1])

    return red_shift_optimum, green_shift_optimum

def alignChannels(red, green, blue, W=30):

    """Given 3 images corresponding to different channels of a color image,
    compute the best aligned result with minimum abberations

    Args:
    red, green, blue - each is a HxW matrix corresponding to an HxW image

    Returns:
    rgb_output - HxWx3 color image output, aligned as desired"""

    red_shift_optimum, green_shift_optimum = recurseNCC(red, green, blue, W)

    red_corrected = shift(red, red_shift_optimum)
    green_corrected = shift(green, green_shift_optimum)

    rgb_output = np.stack([red_corrected, green_corrected, blue], axis=2)

    return rgb_output
