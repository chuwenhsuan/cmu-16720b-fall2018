import numpy as np
from scipy.misc import imsave

from alignChannels import alignChannels

# Problem 1: Image Alignment

if __name__ == '__main__':

    # 1. Load images (all 3 channels)
    red = np.load("../data/red.npy")
    green = np.load("../data/green.npy")
    blue = np.load("../data/blue.npy")

    # 2. Find best alignment
    rgbResult = alignChannels(red, green, blue, W=30)

    # 3. save result to rgb_output.jpg (IN THE "results" FOLDER)
    imsave("../results/rgb_output.jpg", rgbResult)
