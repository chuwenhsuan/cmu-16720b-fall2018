import numpy as np
from numpy.linalg import inv

def warp(im, A, output_shape):

    """ Warps (h,w) image im using affine (3,3) matrix A
    producing (output_shape[0], output_shape[1]) output image
    with warped = A*input, where warped spans 1...output_size.
    Uses nearest neighbor interpolation."""

    L = A[0:2,0:2]
    L_inv = inv(L)
    t = A[0:2,2:]

    warped = np.zeros(im.shape, dtype=np.float32)

    for i in range(im.shape[0]):
        for j in range(im.shape[1]):
            
            lookup_coord = np.array([[i], [j]])
            warped_coord = np.round(L_inv @ (lookup_coord - t)).astype(np.int32)
            warped_y = warped_coord[0][0]
            warped_x = warped_coord[1][0]
            if warped_y < 0 or warped_x < 0 or warped_y >= im.shape[0] or warped_x >= im.shape[1]:
                warped[i][j] = 0
            else:
                warped[i][j] = im[warped_y][warped_x]

    return warped
