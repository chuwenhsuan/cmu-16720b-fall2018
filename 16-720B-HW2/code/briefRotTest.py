import numpy as np
import cv2
import os
from BRIEF import briefLite, briefMatch

import matplotlib.pyplot as plt

if __name__ == '__main__':

    im1 = cv2.imread('../data/model_chickenbroth.jpg')
    locs1, desc1 = briefLite(im1)
    image_center = tuple(np.array(im1.shape[0:2]) / 2)
    angle = []
    match_count = []
    for i in range(36):
        rot_mat = cv2.getRotationMatrix2D(image_center, 10*i, 1.0)
        im2 = cv2.warpAffine(im1, rot_mat, im1.shape[0:2], flags=cv2.INTER_LINEAR)
        locs2, desc2 = briefLite(im2)
        matches = briefMatch(desc1, desc2)
        angle.append(10*i)
        match_count.append(len(matches))
    plt.bar(angle, match_count)
    plt.show()
