import cv2
import numpy as np
from scipy.ndimage.morphology import distance_transform_edt
from planarH import ransacH
from BRIEF import briefLite,briefMatch,plotMatches

def imageStitching(im1, im2, H2to1):
    '''
    Returns a panorama of im1 and im2 using the given 
    homography matrix

    INPUT
        Warps img2 into img1 reference frame using the provided warpH() function
        H2to1 - a 3 x 3 matrix encoding the homography that best matches the linear
                 equation
    OUTPUT
        Blends img1 and warped img2 and outputs the panorama image
    '''
    
    pano_im = cv2.warpPerspective(im2, H2to1, (im1.shape[1], im1.shape[0]), cv2.INTER_NEAREST|cv2.WARP_INVERSE_MAP)
    cv2.imwrite("../results/6_1.jpg", pano_im)
    pano_im[0:im1.shape[0], 0:im1.shape[1], :] = im1

    return pano_im

def imageStitching_noClip(im1, im2, H2to1):
    '''
    Returns a panorama of im1 and im2 using the given 
    homography matrix without cliping.
    ''' 
    
    img1_height, img1_width = im1.shape[0:2]
    img2_height, img2_width = im2.shape[0:2]
    warped_img2_top_left_coord = H2to1 @ np.array([[0.0], [0.0], [1.0]])
    warped_img2_top_left_coord /= (warped_img2_top_left_coord[-1] + 1e-10)
    warped_img2_top_right_coord = H2to1 @ np.array([[img2_width], [0.0], [1.0]])
    warped_img2_top_right_coord /= (warped_img2_top_right_coord[-1] + 1e-10)
    warped_img2_bot_left_coord = H2to1 @ np.array([[0.0], [img2_height], [1.0]])
    warped_img2_bot_left_coord /= (warped_img2_bot_left_coord[-1] + 1e-10)
    warped_img2_bot_right_coord = H2to1 @ np.array([[img2_width], [img2_height], [1.0]])
    warped_img2_bot_right_coord /= (warped_img2_bot_right_coord[-1] + 1e-10)

    leftmost_coord = np.amin(np.concatenate([warped_img2_top_left_coord[0], warped_img2_bot_left_coord[0], [0.0]]))
    rightmost_coord = np.amax(np.concatenate([warped_img2_top_right_coord[0], warped_img2_bot_right_coord[0], [img1_width]]))
    botmost_coord = np.amax(np.concatenate([warped_img2_bot_left_coord[1], warped_img2_bot_right_coord[1], [img1_height]]))
    topmost_coord = np.amin(np.concatenate([warped_img2_top_left_coord[1], warped_img2_top_right_coord[1], [0.0]]))

    stitched_width = int(abs(rightmost_coord - leftmost_coord))
    stitched_height = int(abs(topmost_coord - botmost_coord))

    width_ratio = img1_width/stitched_width
    height_ratio = img1_height/stitched_height

    M = np.array([[width_ratio, 0.0, -leftmost_coord*width_ratio],
                  [0.0, height_ratio, -topmost_coord*height_ratio],
                  [0.0, 0.0, 1.0]])

    pano_im2 = cv2.warpPerspective(im2, M@H2to1, (img1_width, img1_height), cv2.INTER_NEAREST|cv2.WARP_INVERSE_MAP)
    pano_im1 = cv2.warpPerspective(im1, M, (img1_width, img1_height), cv2.INTER_NEAREST|cv2.WARP_INVERSE_MAP)
    pano_im = np.maximum(pano_im2, pano_im1)

    return pano_im

def generatePanorama(im1, im2):
    '''
    Returns a panorama of im1 and im2 without cliping.
    ''' 

    locs1, desc1 = briefLite(im1)
    locs2, desc2 = briefLite(im2)
    matches = briefMatch(desc1, desc2)
    H2to1 = ransacH(matches, locs1, locs2, num_iter=5000, tol=2)
    im3 = imageStitching_noClip(im1, im2, H2to1)

    return im3

if __name__ == '__main__':
    im1 = cv2.imread('../data/incline_L.png')
    im2 = cv2.imread('../data/incline_R.png')
    locs1, desc1 = briefLite(im1)
    locs2, desc2 = briefLite(im2)
    matches = briefMatch(desc1, desc2)
    #plotMatches(im1,im2,matches,locs1,locs2)
    H2to1 = ransacH(matches, locs1, locs2, num_iter=5000, tol=2)
    np.save("../results/q6_1.npy", H2to1)
    #H2to1 = np.load("../results/q6_1.npy")
    pano_im = imageStitching(im1, im2, H2to1)
    pano_im = imageStitching_noClip(im1, im2, H2to1)
    #print(H2to1)
    cv2.imwrite('../results/q6_2_pan.png', pano_im)
    pano_im = generatePanorama(im1, im2)
    cv2.imwrite('../results/q6_3.jpg', pano_im)