import numpy as np
import cv2
import matplotlib.pyplot as plt
from planarH import computeH

CONST_W = np.array([[0.0, 18.2, 18.2, 0.0], [0.0, 0.0, 26.0, 26.0], [0.0, 0.0, 0.0, 0.0]], dtype=np.float32)
CONST_X = np.array([[483, 1704, 2175, 67], [810, 781, 2217, 2286], [1.0, 1.0, 1.0, 1.0]], dtype=np.float32)
CONST_K = np.array([[3043.72, 0.0, 1196], [0.0, 3043.72, 1604], [0.0, 0.0, 1.0]], dtype=np.float32)

def compute_homography(W, X, K):
    '''
    Computes the homography based on given coordinates

    INPUTS
    W - The coordinates in 3D space (3, N)
    X - The projected coordinates (3, N)
    K - Intrinstics matrix (3, 3)

    OUTPUTS
    Hwtox - Homography that maps from W to X
    '''

    K_inverse = np.linalg.inv(K)
    X_prime = K_inverse @ X
    X_prime /= (X_prime[-1] + 1e-10)
    X_prime = X_prime[0:2]
    Hwtox = computeH(X_prime, W[0:2])

    return Hwtox

def compute_extrinsics(K, H):
    '''
    Computes the extrinstics matrix

    INPUTS
    K - Intrinstics matrix (3, 3)
    H - The computed homography (3, 3)

    OUTPUTS
    R - The rotational matrix (3, 3)
    t - The translation vector (3,)
    '''

    u, l, v = np.linalg.svd(H[:,0:2])
    const_mat1 = np.array([[1.0, 0.0], [0.0, 1.0], [0.0, 0.0]])
    r_partial = u @ const_mat1 @ np.transpose(v)
    r1 = np.transpose(r_partial[:,0])
    r2 = np.transpose(r_partial[:,1])
    r3 = np.expand_dims(np.transpose(np.cross(r1, r2)), axis=-1)
    R = np.append(r_partial, r3, axis=1)
    if np.linalg.det(R) == -1:
        R[:,-1] *= -1
    H1 = H[:,0]
    H2 = H[:,1]
    lambda_prime = (np.sum(H1/r1) + np.sum(H2/r2)) / 6.0
    t = H[:,-1] / lambda_prime

    return R, t

def project_extrinsics(K, W, R, t):
    '''
    Projects the coordinates using the calculated extrinstics

    INPUTS
    K - Intrinstics matrix (3, 3)
    W - The coordinates in 3D space (3, N)
    R - The rotational matrix (3, 3)
    t - The translation vector (3,)

    OUTPUTS
    X - The projected coordinates (2, N)
    '''

    const_ones = np.ones((1, W.shape[1]), dtype=np.float32)
    W_homo = np.append(W, const_ones, axis=0)
    concat_Rt = np.append(R, np.expand_dims(t, -1), axis=1)
    X_homo = K @ concat_Rt @ W_homo
    X = (X_homo / (X_homo[-1]+1e-10))[0:2]

    return X

if __name__ == '__main__':

    CONST_SHIFT_X = 4.4
    CONST_SHIFT_Y = 0.0
    CONST_SHIFT_Z = -9.5

    img = cv2.imread("../data/prince_book.jpeg")[:, :, ::-1]

    Hktox = compute_homography(CONST_W, CONST_X, CONST_K)
    R, t = compute_extrinsics(CONST_K, Hktox)

    coords = []
    with open('../data/sphere.txt','r') as f:
        for line in f:
            coords.append(line.split())
    coords = np.stack(coords, axis=0).astype(np.float32)
    coords[0, :] += CONST_SHIFT_X
    coords[1, :] += CONST_SHIFT_Y
    coords[2, :] += CONST_SHIFT_Z
    
    X = project_extrinsics(CONST_K, coords, R, t).astype(int)

    fig = plt.figure()
    plt.imshow(img)
    plt.plot(X[0,:], X[1,:], 'y.', markersize=3)
    plt.draw()
    plt.waitforbuttonpress(0)
    plt.close(fig)