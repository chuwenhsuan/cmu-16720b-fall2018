import numpy as np
import cv2
from BRIEF import briefLite, briefMatch

def computeH(p1, p2):
    '''
    INPUTS:
        p1 and p2 - Each are size (2 x N) matrices of corresponding (x, y)'  
                 coordinates between two images
    OUTPUTS:
     H2to1 - a 3 x 3 matrix encoding the homography that best matches the linear 
            equation
    '''
    assert(p1.shape[1]==p2.shape[1])
    assert(p1.shape[0]==2)
    
    num_points = p1.shape[1]
    mat_A = np.zeros((2*num_points, 9), dtype=np.float32)

    for i in range(num_points):

        mat_A[2*i][0:2] = p2[:, i]
        mat_A[2*i][2] = 1.0
        mat_A[2*i][6] = - (p2[0, i]*p1[0, i])
        mat_A[2*i][7] = - (p2[1, i]*p1[0, i])
        mat_A[2*i][8] = - (p1[0, i])

        mat_A[2*i+1][3:5] = p2[:, i]
        mat_A[2*i+1][5] = 1.0
        mat_A[2*i+1][6] = - (p2[0, i]*p1[1, i])
        mat_A[2*i+1][7] = - (p2[1, i]*p1[1, i])
        mat_A[2*i+1][8] = - (p1[1, i])

    mat_A_T = np.transpose(mat_A)
    mat_product = mat_A_T @ mat_A
    u, s, v = np.linalg.svd(mat_product, full_matrices=False)

    vec_h = v[-1]
    H2to1 = np.reshape(vec_h, (3, 3))

    return H2to1

def ransacH(matches, locs1, locs2, num_iter=5000, tol=2):
    '''
    Returns the best homography by computing the best set of matches using
    RANSAC
    INPUTS
        locs1 and locs2 - matrices specifying point locations in each of the images
        matches - matrix specifying matches between these two sets of point locations
        nIter - number of iterations to run RANSAC
        tol - tolerance value for considering a point to be an inlier

    OUTPUTS
        bestH - homography matrix with the most inliers found during RANSAC
    ''' 
    
    best_inliers_idx = []

    for i in range(num_iter):

        rand_inliers_idx = np.random.randint(0, matches.shape[0], 4)
        matches_subset = np.take(matches, rand_inliers_idx, axis=0)
        p1_subset = np.take(locs1, matches_subset[:,0], axis=0)[:, 0:2]
        p2_subset = np.take(locs2, matches_subset[:,1], axis=0)[:, 0:2]
        p1_subset = np.transpose(p1_subset)
        p2_subset = np.transpose(p2_subset)

        H2to1 = computeH(p1_subset, p2_subset)
        
        curr_inliers_idx = []
        inlier_count = 0
        for match in matches:

            point1 = np.append(locs1[match[0], 0:2], 1.0)
            point2 = np.append(locs2[match[1], 0:2], 1.0)
            point2to1 = H2to1 @ point2
            point2to1 /= (point2to1[-1] + 1e-10)

            if np.linalg.norm(point2to1-point1) < tol:

                inlier_count += 1
                curr_inliers_idx.append(match)

        if inlier_count > len(best_inliers_idx):
            best_inliers_idx = curr_inliers_idx
    
    matches_subset = np.stack(best_inliers_idx, axis=0)
    p1_subset = np.take(locs1, matches_subset[:,0], axis=0)[:, 0:2]
    p2_subset = np.take(locs2, matches_subset[:,1], axis=0)[:, 0:2]
    p1_subset = np.transpose(p1_subset)
    p2_subset = np.transpose(p2_subset)

    bestH = computeH(p1_subset, p2_subset)

    return bestH

if __name__ == '__main__':
    im1 = cv2.imread('../data/model_chickenbroth.jpg')
    im2 = cv2.imread('../data/chickenbroth_01.jpg')
    locs1, desc1 = briefLite(im1)
    locs2, desc2 = briefLite(im2)
    matches = briefMatch(desc1, desc2)
    ransacH(matches, locs1, locs2, num_iter=5000, tol=2)

