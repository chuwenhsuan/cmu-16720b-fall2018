import os
import numpy as np
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

from vae import VAE

EPOCH = 10
BATCH_SIZE = 64
LEARNING_RATE = 1e-3

VISUALIZE_DIR = "./recon_mnist/"
SAVE_PATH = "./vae_mnist.pth"

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

train_loader = torch.utils.data.DataLoader(
  torchvision.datasets.MNIST('/files/', train=True, download=True,
                             transform=torchvision.transforms.ToTensor(),
                             ),
  batch_size=BATCH_SIZE, shuffle=True)

test_loader = torch.utils.data.DataLoader(
  torchvision.datasets.MNIST('/files/', train=False, download=True,
                             transform=torchvision.transforms.ToTensor()
                             ),
  batch_size=BATCH_SIZE, shuffle=True)

def calc_loss(recon, x, mu, logvar):
    
    loss_recon = torch.sum((recon - x) ** 2, dim=(1, 2, 3))
    loss_kld = -0.5 * torch.sum((1 + logvar - mu ** 2 - torch.exp(logvar)), dim=1)

    return torch.mean(loss_recon + 0.5*loss_kld)

def train(vae):

    print("Start Training")

    vae.train()
    optimizer = optim.Adam(vae.parameters(), lr=LEARNING_RATE)

    for epoch in range(EPOCH):

        total_loss = 0.

        for data, _ in train_loader:

            data = data.to(device)

            optimizer.zero_grad()
            
            recon, _, mu, logvar = vae(data)

            loss = calc_loss(recon, data, mu, logvar)
            loss.backward()
            optimizer.step()

            total_loss += loss.item()

        avg_loss = total_loss / (len(train_loader))

        print("[Epoch %d] Training Loss: %.3f" % (epoch + 1, avg_loss))
        torch.save(vae, SAVE_PATH)
        total_loss = 0.0

    print("Finished Training")

def test():

    print("Start Evaluation")

    if not os.path.isfile(SAVE_PATH):
        print("Saved weights not found.")
        return

    if not os.path.exists(VISUALIZE_DIR): os.mkdir(VISUALIZE_DIR)

    vae = torch.load(SAVE_PATH).to(device)
    vae.eval()

    for data, _ in test_loader:

        recon, fv, _, _ = vae(data.to(device))

        for idx, (img_recon, img_orig) in enumerate(zip(recon, data)):
            
            save_path_orig = VISUALIZE_DIR + "img_" + str(idx).zfill(4) + "_o.png"
            save_path_recon = VISUALIZE_DIR + "img_" + str(idx).zfill(4) + "_r.png"
            torchvision.utils.save_image(img_orig, save_path_orig)
            torchvision.utils.save_image(img_recon, save_path_recon)

        sample1 = torch.unsqueeze(data[0], dim=0)
        sample2 = torch.unsqueeze(data[1], dim=0)

        recon1, fv1, _, _ = vae(sample1.to(device))
        torchvision.utils.save_image(recon1, VISUALIZE_DIR + "img_ip_" + str(0).zfill(2) + ".png")
        recon2, fv2, _, _ = vae(sample2.to(device))
        torchvision.utils.save_image(recon2, VISUALIZE_DIR + "img_ip_" + str(10).zfill(2) + ".png")

        for i in range(1, 20, 1):
            
            fv = fv1 + (i/19) * (fv2 - fv1)
            recon = vae.decode(fv.to(device))
            torchvision.utils.save_image(recon, VISUALIZE_DIR + "img_ip_" + str(i).zfill(2) + ".png")
        
        break
        
    print("Finished Evaluation")

if __name__ == '__main__':

    #vae = VAE().to(device)
    #train(vae)

    test()