import numpy as np
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import matplotlib.pyplot as plt

class VAE(nn.Module):

    def __init__(self):

        super(VAE, self).__init__()

        self.enc_fc1 = nn.Linear(560, 256)
        self.enc_fc_mu = nn.Linear(256, 8)
        self.enc_fc_logvar = nn.Linear(256, 8)
        
        self.dec_fc1 = nn.Linear(8, 256)
        self.dec_fc2 = nn.Linear(256, 560)
    
    def encode(self, x):

        x = F.relu(self.enc_fc1(x))
        mu = self.enc_fc_mu(x)
        logvar = self.enc_fc_logvar(x)

        return mu, logvar

    def reparametrise(self, mu, logvar):

        if self.training == True:

            std = torch.exp(0.5*logvar)
            eps = torch.randn_like(std)
            
            return (mu + std * eps)
        
        else:

            return mu

    def decode(self, x):

        x = F.relu(self.dec_fc1(x))
        x = torch.sigmoid(self.dec_fc2(x))

        return x
        
    def forward(self, x):

        mu, logvar = self.encode(x)
        fv = self.reparametrise(mu, logvar)
        recon = self.decode(fv)

        return recon, fv, mu, logvar