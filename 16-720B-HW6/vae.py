import numpy as np
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import matplotlib.pyplot as plt

class VAE(nn.Module):

    def __init__(self):

        super(VAE, self).__init__()

        self.enc_conv1 = nn.Conv2d(1, 32, kernel_size=3, stride=2)
        self.enc_conv2 = nn.Conv2d(32, 64, kernel_size=3, stride=2)
        self.enc_conv3 = nn.Conv2d(64, 128, kernel_size=3, stride=2)
        self.enc_fc_mu = nn.Linear(512, 32)
        self.enc_fc_logvar = nn.Linear(512, 32)
        
        self.dec_fc1 = nn.Linear(32, 512)
        self.dec_conv1 = nn.ConvTranspose2d(128, 64, kernel_size=3, stride=2, output_padding=1)
        self.dec_conv2 = nn.ConvTranspose2d(64, 32, kernel_size=3, stride=2)
        self.dec_conv3 = nn.ConvTranspose2d(32, 1, kernel_size=3, stride=2, output_padding=1)
    
    def encode(self, x):

        x = F.relu(self.enc_conv1(x))
        x = F.relu(self.enc_conv2(x))
        x = F.relu(self.enc_conv3(x))
        x = x.view(-1, 512)
        mu = self.enc_fc_mu(x)
        logvar = self.enc_fc_logvar(x)

        return mu, logvar

    def reparametrise(self, mu, logvar):

        if self.training == True:

            std = torch.exp(0.5*logvar)
            eps = torch.randn_like(std)
            
            return (mu + std * eps)
        
        else:

            return mu

    def decode(self, x):

        x = F.relu(self.dec_fc1(x))
        x = x.view(-1, 128, 2, 2)
        x = F.relu(self.dec_conv1(x))
        x = F.relu(self.dec_conv2(x))
        x = torch.sigmoid(self.dec_conv3(x))

        return x
        
    def forward(self, x):

        mu, logvar = self.encode(x)
        fv = self.reparametrise(mu, logvar)
        recon = self.decode(fv)

        return recon, fv, mu, logvar