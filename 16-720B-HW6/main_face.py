import os
import scipy.io
import numpy as np
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

from vae2 import VAE

EPOCH = 20
BATCH_SIZE = 32
LEARNING_RATE = 1e-3

VISUALIZE_DIR = "./recon_face/"
SAVE_PATH = "./vae_face.pth"

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

data = scipy.io.loadmat('./frey_rawface.mat')["ff"]
data = data.T.reshape((-1, 28*20)) / 255.0

def get_random_batch(data):

    batches = []

    shuffled_data = np.random.shuffle(data)
    data_len = len(data)
    idx = 0

    for idx in range(data_len//BATCH_SIZE):

        batch = np.array(data[BATCH_SIZE*idx:BATCH_SIZE*(idx+1)])
        batches.append(batch)

    batch = np.array(data[BATCH_SIZE*idx:])

    if not len(data[BATCH_SIZE*idx:]) == 0: batches.append(batch)

    return batches

def calc_loss(recon, x, mu, logvar):
    
    loss_recon = torch.sum((recon - x) ** 2, dim=1)
    loss_kld = -0.5 * torch.sum((1 + logvar - mu ** 2 - torch.exp(logvar)), dim=1)

    return torch.mean(loss_recon + 0.1*loss_kld)

def train(vae):

    print("Start Training")

    vae.train()
    optimizer = optim.Adam(vae.parameters(), lr=LEARNING_RATE)

    train_batches = get_random_batch(data)

    for epoch in range(EPOCH):

        total_loss = 0.

        for x in train_batches:

            optimizer.zero_grad()

            x = Variable(torch.from_numpy(x)).float().to(device)

            recon, _, mu, logvar = vae(x)

            loss = calc_loss(recon, x, mu, logvar)
            loss.backward()
            optimizer.step()

            total_loss += loss.item()

        avg_loss = total_loss / (len(train_batches))

        print("[Epoch %d] Training Loss: %.3f" % (epoch + 1, avg_loss))
        torch.save(vae, SAVE_PATH)
        total_loss = 0.0

    print("Finished Training")

def test():

    print("Start Evaluation")

    if not os.path.isfile(SAVE_PATH):
        print("Saved weights not found.")
        return

    if not os.path.exists(VISUALIZE_DIR): os.mkdir(VISUALIZE_DIR)

    vae = torch.load(SAVE_PATH).to(device)
    vae.eval()

    test_batches = get_random_batch(data)

    for x in test_batches:

        x = Variable(torch.from_numpy(x)).float()

        recon, fv, _, _ = vae(x.to(device))

        for idx, (img_recon, img_orig) in enumerate(zip(recon, x)):
            
            save_path_orig = VISUALIZE_DIR + "img_" + str(idx).zfill(4) + "_o.png"
            save_path_recon = VISUALIZE_DIR + "img_" + str(idx).zfill(4) + "_r.png"
            torchvision.utils.save_image(img_orig.view(-1, 1, 28, 20), save_path_orig)
            torchvision.utils.save_image(img_recon.view(-1, 1, 28, 20), save_path_recon)

        sample1 = torch.unsqueeze(x[0], dim=0)
        sample2 = torch.unsqueeze(x[1], dim=0)

        recon1, fv1, _, _ = vae(sample1.to(device))
        torchvision.utils.save_image(recon1.view(-1, 1, 28, 20), VISUALIZE_DIR + "img_ip_" + str(0).zfill(2) + ".png")
        recon2, fv2, _, _ = vae(sample2.to(device))
        torchvision.utils.save_image(recon2.view(-1, 1, 28, 20), VISUALIZE_DIR + "img_ip_" + str(20).zfill(2) + ".png")

        for i in range(1, 20, 1):
            
            fv = fv1 + (i/19) * (fv2 - fv1)
            recon = vae.decode(fv.to(device))
            torchvision.utils.save_image(recon.view(-1, 1, 28, 20), VISUALIZE_DIR + "img_ip_" + str(i).zfill(2) + ".png")
        
        break
        
    print("Finished Evaluation")

if __name__ == '__main__':

    #vae = VAE().to(device)
    #train(vae)

    test()