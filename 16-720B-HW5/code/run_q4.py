import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches

import skimage
import skimage.measure
import skimage.color
import skimage.restoration
import skimage.io
import skimage.filters
import skimage.morphology
import skimage.segmentation

from nn import *
from q4 import *
# do not include any more libraries here!
# no opencv, no sklearn, etc!
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=UserWarning)

for img in os.listdir('../images'):
    im1 = skimage.img_as_float(skimage.io.imread(os.path.join('../images',img)))
    bboxes, bw = findLetters(im1)

    plt.imshow(bw)
    for bbox in bboxes:
        minr, minc, maxr, maxc = bbox
        rect = matplotlib.patches.Rectangle((minc, minr), maxc - minc, maxr - minr,
                                fill=False, edgecolor='red', linewidth=2)
        plt.gca().add_patch(rect)
    plt.show()
    # find the rows using..RANSAC, counting, clustering, etc.
    r_idxs = []
    c_idxs = []
    idx = []
    max_length = 0.
    for i, bbox in enumerate(bboxes):
        minr, minc, maxr, maxc = bbox
        midr = minr + (maxr - minr) / 2.
        midc = minc + (maxc - minc) / 2.
        r_idxs.append(midr)
        c_idxs.append(midc)
    count_r_idxs = [0]
    idx = 0
    newline = []
    for i in range(1, len(r_idxs), 1):
        if np.abs(r_idxs[i] - r_idxs[i-1]) > 80:
            idx += 1
            newline.append(i-1)
        count_r_idxs.append(idx)
    r_idxs = np.stack(count_r_idxs)
    c_idxs = np.stack(c_idxs)
    order_idxs = np.lexsort((c_idxs, r_idxs))
    # crop the bounding boxes
    crops = []
    for idx in order_idxs:
        minr, minc, maxr, maxc = bboxes[idx]
        crop = bw[minr:maxr, minc:maxc]
        diff = (maxc - minc) - (maxr - minr)
        if diff > 0:
            crop = np.pad(crop, ((diff//2, diff//2), (0, 0)), 'constant', constant_values=1.0)
        else:
            crop = np.pad(crop, ((0, 0), (-diff//2, -diff//2)), 'constant', constant_values=1.0)
        crop = skimage.morphology.binary_erosion(crop).astype(np.float)
        crop = np.pad(crop, ((10, 10), (10, 10)), 'constant', constant_values=1.0)
        resized_crop = skimage.transform.resize(crop, (32, 32))
        #skimage.io.imshow(resized_crop)
        #skimage.io.show()
        flattened_crop = np.ndarray.flatten(resized_crop.T)
        crops.append(np.expand_dims(flattened_crop, axis=0))
    # note.. before you flatten, transpose the image (that's how the dataset is!)
    # consider doing a square crop, and even using np.pad() to get your images looking more like the dataset
    
    # load the weights
    # run the crops through your neural network and print them out
    import pickle
    import string
    letters = np.array([_ for _ in string.ascii_uppercase[:26]] + [str(_) for _ in range(10)])
    params = pickle.load(open('q3_weights.pickle','rb'))
    for i, crop in enumerate(crops):
        post_act = forward(crop, params, "layer1")
        pred = forward(post_act, params, "output", softmax)
        pred_idx = np.argmax(pred)
        # What do the idx correspond to?
        if pred_idx < 26:
            print (chr(pred_idx+65), end='')
        else:
            print (pred_idx-26, end='')
        if i in newline:
            print ('')
    print ('\n')
