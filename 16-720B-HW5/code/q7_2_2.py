import numpy as np
import scipy.io
from nn import *
from glob import glob
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import matplotlib.pyplot as plt

max_iters = 10
# pick a batch size, learning rate
batch_size = 64
learning_rate = 1e-3
num_class = 17

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=UserWarning)


train_dir = "../data/oxford-flowers17/train/"
test_dir = "../data/oxford-flowers17/test/"

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

normalize = torchvision.transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                             std=[0.229, 0.224, 0.225])
train_dataset = torchvision.datasets.ImageFolder(
                                                train_dir,
                                                torchvision.transforms.Compose([
                                                    torchvision.transforms.Scale(256),
                                                    torchvision.transforms.RandomResizedCrop(224),
                                                    torchvision.transforms.RandomHorizontalFlip(),
                                                    torchvision.transforms.ToTensor(),
                                                    normalize,
                                                ]))
test_dataset = torchvision.datasets.ImageFolder(
                                                test_dir,
                                                torchvision.transforms.Compose([
                                                    torchvision.transforms.Scale(256),
                                                    torchvision.transforms.RandomResizedCrop(224),
                                                    torchvision.transforms.RandomHorizontalFlip(),
                                                    torchvision.transforms.ToTensor(),
                                                    normalize,
                                                ]))

train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=True)

class nn722(nn.Module):

    def __init__(self):

        super(nn722, self).__init__()

        self.conv1 = nn.Conv2d(3, 32, kernel_size=3, stride=1, padding=(1, 1))
        self.pool1 = nn.AvgPool2d(kernel_size=2, stride=2)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=3, stride=1, padding=(1, 1))
        self.pool2 = nn.AvgPool2d(kernel_size=2, stride=2)
        self.conv3 = nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=(1, 1))
        self.pool3 = nn.AvgPool2d(kernel_size=2, stride=2)
        self.fc1 = nn.Linear(28*28*128, 1024)
        self.fc2 = nn.Linear(1024, 47)

    def forward(self, x):

        x = x.view(-1, 3, 224, 224)
        x = F.relu(self.conv1(x))
        x = self.pool1(x)
        x = F.relu(self.conv2(x))
        x = self.pool2(x)
        x = F.relu(self.conv3(x))
        x = self.pool3(x)
        x = x.view(-1, 28*28*128)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)

        return x


nn722 = nn722().to(device)

criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(nn722.parameters(), lr=learning_rate)

train_loss_list = []
train_acc_list = []
test_loss_list = []
test_acc_list = []

for epoch in range(max_iters):  # loop over the dataset multiple times

    train_total_loss = 0.
    train_avg_acc = 0.
    for batch_idx, (data, target) in enumerate(train_loader):

        gt = target.data.numpy()

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = nn722(data.to(device))

        pred = np.argmax(outputs.cpu().data.numpy(), axis=1)
        correct = np.equal(gt, pred).astype(np.float32)
        acc = np.sum(correct) / correct.shape[0]

        loss = criterion(outputs, target.to(device))
        loss.backward()
        optimizer.step()

        # print statistics
        train_avg_acc += acc
        train_total_loss += loss.item()

    train_avg_acc /= (batch_idx+1)
    train_avg_loss = train_total_loss / (batch_idx+1)
    train_loss_list.append(train_avg_loss)
    train_acc_list.append(train_avg_acc)

    print('[Epoch %d] Training Loss: %.3f, Training Acc: %.3f' % (epoch + 1, train_avg_loss, train_avg_acc))
    train_total_loss = 0.0
    train_avg_acc = 0.0

    test_total_loss = 0.
    test_avg_acc = 0.
    for batch_idx, (data, target) in enumerate(test_loader):

        gt = target.data.numpy()

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = nn722(data.to(device))

        pred = np.argmax(outputs.cpu().data.numpy(), axis=1)
        correct = np.equal(gt, pred).astype(np.float32)
        acc = np.sum(correct) / correct.shape[0]

        loss = criterion(outputs, target.to(device))

        # print statistics
        test_avg_acc += acc
        test_total_loss += loss.item()

    test_avg_acc /= (batch_idx+1)
    test_avg_loss = test_total_loss / (batch_idx+1)
    test_loss_list.append(test_avg_loss)
    test_acc_list.append(test_avg_acc)

    print('[Epoch %d] Testing Loss: %.3f, Testing Acc: %.3f' % (epoch + 1, test_avg_loss, test_avg_acc))
    test_total_loss = 0.0
    test_avg_acc = 0.0

print('Finished Training')

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(np.arange(max_iters), train_acc_list, 'r-')
ax.plot(np.arange(max_iters), test_acc_list, 'b-')
fig.show()

fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
ax2.plot(np.arange(max_iters), train_loss_list, 'r-')
ax2.plot(np.arange(max_iters), test_loss_list, 'b-')
fig2.show()

plt.show()