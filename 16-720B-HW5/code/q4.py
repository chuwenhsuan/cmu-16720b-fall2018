import numpy as np

import skimage
import skimage.measure
import skimage.color
import skimage.restoration
import skimage.filters
import skimage.morphology
import skimage.segmentation

# takes a color image
# returns a list of bounding boxes and black_and_white image
def findLetters(image):

    bboxes = []

    # insert processing in here
    AREA_THRESH = 180
    image = skimage.color.rgb2gray(image)
    thresh = skimage.filters.threshold_otsu(image)
    bw = skimage.morphology.binary_dilation(image < thresh)
    bw = skimage.morphology.binary_dilation(bw)
    cleared = skimage.segmentation.clear_border(bw)
    label_image = skimage.measure.label(cleared)

    for region in skimage.measure.regionprops(label_image):
        if region.area >= AREA_THRESH:
            minr, minc, maxr, maxc = region.bbox
            bboxes.append((minr, minc, maxr, maxc))
    
    return bboxes, np.invert(bw)