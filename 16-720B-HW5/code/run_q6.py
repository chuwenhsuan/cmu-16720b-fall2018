import numpy as np
import scipy.io
from nn import *
import matplotlib.pyplot as plt
from skimage.measure import compare_psnr as psnr

train_data = scipy.io.loadmat('../data/nist36_train.mat')
valid_data = scipy.io.loadmat('../data/nist36_valid.mat')

# we don't need labels now!
train_x = train_data['train_data']
valid_x = valid_data['valid_data']
valid_y = valid_data['valid_labels']

CLASS_IDX = [0, 3, 14, 17, 29]

dim = 32
# do PCA
mean_train = np.expand_dims(np.mean(train_x, axis=0), axis=0)
mean_val = np.expand_dims(np.mean(valid_x, axis=0), axis=0)
mean_train_data = train_x - mean_train
mean_val_data = valid_x - mean_val
U, S, V = np.linalg.svd(train_x, full_matrices=False)
proj_mat = V[0:dim, :].T

print ("Rank of projection matrix: " + str(np.linalg.matrix_rank(proj_mat)))
print ("Shape of projection matrix: " + str(proj_mat.shape))

# rebuild a low-rank version
lrank = mean_train_data @ proj_mat

# rebuild it
recon = lrank @ proj_mat.T + mean_train

# build valid dataset
recon_valid = mean_val_data @ proj_mat @ proj_mat.T + mean_val
count = 0
for i in range(len(valid_x)):

    if len(CLASS_IDX) == 0: break

    if count >= 2:
        del CLASS_IDX[0]
        count = 0

    if np.argmax(valid_y[i]) in CLASS_IDX:
        plt.subplot(2,1,1)
        plt.imshow(valid_x[i].reshape(32,32).T)
        plt.subplot(2,1,2)
        plt.imshow(recon_valid[i].reshape(32,32).T)
        plt.show()

        count += 1

total = []
for pred,gt in zip(recon_valid,valid_x):
    total.append(psnr(gt,pred))
print(np.array(total).mean())