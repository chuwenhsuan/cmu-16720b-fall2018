import numpy as np
import scipy.io
from nn import *
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import matplotlib.pyplot as plt

train_data = scipy.io.loadmat('../data/nist36_train.mat')
valid_data = scipy.io.loadmat('../data/nist36_valid.mat')

train_x, train_y = train_data['train_data'], train_data['train_labels']
valid_x, valid_y = valid_data['valid_data'], valid_data['valid_labels']

max_iters = 50
# pick a batch size, learning rate
batch_size = 64
learning_rate = 1e-1
hidden_size = 64

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

train_batches = get_random_batches(train_x,train_y,batch_size)
val_batches = get_random_batches(valid_x,valid_y,batch_size)

class nn711(nn.Module):

    def __init__(self):

        super(nn711, self).__init__()

        self.fc1 = nn.Linear(1024, hidden_size)
        self.fc2 = nn.Linear(hidden_size, 36)

    def forward(self, x):

        x = torch.sigmoid(self.fc1(x))
        x = self.fc2(x)

        return x

nn711 = nn711().to(device)

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(nn711.parameters(), lr=learning_rate)

loss_list = []
acc_list = []

for epoch in range(max_iters):  # loop over the dataset multiple times

    total_loss = 0.
    avg_acc = 0.
    for xb, yb in train_batches:

        gt = np.argmax(yb, axis=1)

        x = Variable(torch.from_numpy(xb)).float()
        y = Variable(torch.from_numpy(gt)).long()
        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = nn711(x.to(device))

        pred = np.argmax(outputs.cpu().data.numpy(), axis=1)
        correct = np.equal(gt, pred).astype(np.float32)
        acc = np.sum(correct) / correct.shape[0]

        loss = criterion(outputs, y.to(device))
        loss.backward()
        optimizer.step()

        # print statistics
        avg_acc += acc
        total_loss += loss.item()

    avg_acc /= len(train_batches)
    avg_loss = total_loss / len(train_batches)
    loss_list.append(avg_loss)
    acc_list.append(avg_acc)

    print('[Epoch %d] Training Loss: %.3f, Training Acc: %.3f' % (epoch + 1, avg_loss, avg_acc))
    total_loss = 0.0
    avg_acc = 0.0

print('Finished Training')

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(np.arange(max_iters), acc_list, 'r-')
fig.show()

fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
ax2.plot(np.arange(max_iters), loss_list, 'r-')
fig2.show()

plt.show()