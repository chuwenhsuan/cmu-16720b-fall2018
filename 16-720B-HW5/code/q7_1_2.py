import numpy as np
import scipy.io
from nn import *
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import matplotlib.pyplot as plt

max_iters = 3
# pick a batch size, learning rate
batch_size = 64
learning_rate = 1e-2

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

train_loader = torch.utils.data.DataLoader(
  torchvision.datasets.MNIST('/files/', train=True, download=True,
                             transform=torchvision.transforms.Compose([
                               torchvision.transforms.ToTensor(),
                               torchvision.transforms.Normalize(
                                 (0.1307,), (0.3081,))
                             ])),
  batch_size=batch_size, shuffle=True)

test_loader = torch.utils.data.DataLoader(
  torchvision.datasets.MNIST('/files/', train=False, download=True,
                             transform=torchvision.transforms.Compose([
                               torchvision.transforms.ToTensor(),
                               torchvision.transforms.Normalize(
                                 (0.1307,), (0.3081,))
                             ])),
  batch_size=batch_size, shuffle=True)

class nn712(nn.Module):

    def __init__(self):

        super(nn712, self).__init__()

        self.conv1 = nn.Conv2d(1, 16, kernel_size=3, stride=2)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=3, stride=2)
        self.fc1 = nn.Linear(1152, 32)
        self.fc2 = nn.Linear(32, 10)

    def forward(self, x):

        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = x.view(-1, 1152)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)

        return x

nn712 = nn712().to(device)

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(nn712.parameters(), lr=learning_rate)

loss_list = []
acc_list = []

for epoch in range(max_iters):  # loop over the dataset multiple times

    total_loss = 0.
    avg_acc = 0.
    for batch_idx, (data, target) in enumerate(train_loader):

        gt = target.data.numpy()

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = nn712(data.to(device))

        pred = np.argmax(outputs.cpu().data.numpy(), axis=1)
        correct = np.equal(gt, pred).astype(np.float32)
        acc = np.sum(correct) / correct.shape[0]

        loss = criterion(outputs, target.to(device))
        loss.backward()
        optimizer.step()

        # print statistics
        avg_acc += acc
        total_loss += loss.item()

    avg_acc /= (batch_idx+1)
    avg_loss = total_loss / (batch_idx+1)
    loss_list.append(avg_loss)
    acc_list.append(avg_acc)

    print('[Epoch %d] Training Loss: %.3f, Training Acc: %.3f' % (epoch + 1, avg_loss, avg_acc))
    total_loss = 0.0
    avg_acc = 0.0

print('Finished Training')

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(np.arange(max_iters), acc_list, 'r-')
fig.show()

fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
ax2.plot(np.arange(max_iters), loss_list, 'r-')
fig2.show()

plt.show()