import numpy as np
import scipy.io
from nn import *

train_data = scipy.io.loadmat('../data/nist36_train.mat')
valid_data = scipy.io.loadmat('../data/nist36_valid.mat')

train_x, train_y = train_data['train_data'], train_data['train_labels']
valid_x, valid_y = valid_data['valid_data'], valid_data['valid_labels']

max_iters = 50
# pick a batch size, learning rate
batch_size = 64
learning_rate = 1e-3
hidden_size = 64

batches = get_random_batches(train_x,train_y,batch_size)
batch_num = len(batches)

params = {}

# initialize layers here

initialize_weights(1024,hidden_size,params,'layer1')
initialize_weights(hidden_size,36,params,'output')

# with default settings, you should get loss < 150 and accuracy > 80%

train_batches = get_random_batches(train_x,train_y,batch_size)
val_batches = get_random_batches(valid_x,valid_y,batch_size)

train_acc_list = []
train_loss_list = []
val_acc_list = []
val_loss_list = []
for itr in range(max_iters):
    train_total_loss = 0
    train_avg_acc = 0
    for xb,yb in train_batches:

        post_act = forward(xb, params, "layer1")
        pred = forward(post_act, params, "output", softmax)

        loss, acc = compute_loss_and_acc(yb, pred)
        train_total_loss += loss / len(xb)
        train_avg_acc += acc

        delta1 = pred - yb
        delta2 = backwards(delta1, params, "output", linear_deriv)
        _ = backwards(delta2, params, "layer1", sigmoid_deriv)

        params['W' + "layer1"] -= learning_rate *  params['grad_W' + "layer1"]
        params['b' + "layer1"] -= learning_rate *  params['grad_b' + "layer1"]
        params['W' + "output"] -= learning_rate *  params['grad_W' + "output"]
        params['b' + "output"] -= learning_rate *  params['grad_b' + "output"]
        
    train_avg_acc /= len(train_batches)
    train_avg_loss = train_total_loss / len(train_batches)
    train_acc_list.append(train_avg_acc)
    train_loss_list.append(train_avg_loss)
        
    if itr % 2 == 0:
        print("itr: {:02d} \t loss: {:.2f} \t acc : {:.2f}".format(itr,train_avg_loss,train_avg_acc))
    # run on validation set and report accuracy! should be above 75%
    
    val_total_loss = 0
    val_avg_acc = 0
    for xb,yb in val_batches:

        post_act = forward(xb, params, "layer1")
        pred = forward(post_act, params, "output", softmax)

        loss, acc = compute_loss_and_acc(yb, pred)
        val_total_loss += loss / len(xb)
        val_avg_acc += acc
    
    val_avg_acc /= len(val_batches)
    val_acc_list.append(val_avg_acc)
    val_loss_list.append(val_total_loss / len(val_batches))

    print('Validation accuracy: ',val_avg_acc)

train_acc_list = np.stack(train_acc_list)
train_loss_list = np.stack(train_loss_list)
val_acc_list = np.stack(val_acc_list)
val_loss_list = np.stack(val_loss_list)

if False: # view the data
    for crop, label in zip(xb, yb):
        import matplotlib.pyplot as plt
        print (np.argmax(label))
        plt.imshow(crop.reshape(32,32).T)
        plt.show()
import pickle
saved_params = {k:v for k,v in params.items() if '_' not in k}
with open('q3_weights.pickle', 'wb') as handle:
    pickle.dump(saved_params, handle, protocol=pickle.HIGHEST_PROTOCOL)

# Q3.1.1 & Q3.1.3
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(np.arange(max_iters), train_acc_list, 'r-')
ax.plot(np.arange(max_iters), val_acc_list, 'b-')
fig.show()

fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
ax2.plot(np.arange(max_iters), train_loss_list, 'r-')
ax2.plot(np.arange(max_iters), val_loss_list, 'b-')
fig2.show()

fig3 = plt.figure()
grid1 = ImageGrid(fig3, 111, (8, 8))
for i in range(64):
    grid1[i].imshow(params["Wlayer1"][:, i].reshape(32, 32))
fig3.show()

params2 = {}
initialize_weights(1024,hidden_size,params2,'layer1')

fig4 = plt.figure()
grid2 = ImageGrid(fig4, 111, (8, 8))
for i in range(64):
    grid2[i].imshow(params2["Wlayer1"][:, i].reshape(32, 32))
fig4.show()

# Q3.1.4
confusion_matrix = np.zeros((train_y.shape[1],train_y.shape[1]))
for xb,yb in val_batches:

    post_act = forward(xb, params, "layer1")
    pred = forward(post_act, params, "output", softmax)

    for gt, pred_label in zip(yb, pred):
        confusion_matrix[np.argmax(gt), np.argmax(pred_label)] += 1.0

import string
fig5 = plt.figure()
plt.imshow(confusion_matrix,interpolation='nearest')
plt.grid(True)
plt.xticks(np.arange(36),string.ascii_uppercase[:26] + ''.join([str(_) for _ in range(10)]))
plt.yticks(np.arange(36),string.ascii_uppercase[:26] + ''.join([str(_) for _ in range(10)]))
fig5.show()
plt.show()