import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches

import skimage
import skimage.measure
import skimage.color
import skimage.restoration
import skimage.io
import skimage.filters
import skimage.morphology
import skimage.segmentation
from nn import *

import os
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable
import matplotlib.pyplot as plt
from q4 import *

max_iters = 5
# pick a batch size, learning rate
batch_size = 64
learning_rate = 5e-3

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=UserWarning)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

train_loader = torch.utils.data.DataLoader(
  torchvision.datasets.EMNIST('/files/', split='balanced', train=True, download=True,
                             transform=torchvision.transforms.Compose([
                               torchvision.transforms.ToTensor(),
                               torchvision.transforms.Normalize(
                                 (0.1307,), (0.3081,))
                             ])),
  batch_size=batch_size, shuffle=True)

test_loader = torch.utils.data.DataLoader(
  torchvision.datasets.EMNIST('/files/', split='balanced', train=False, download=True,
                             transform=torchvision.transforms.Compose([
                               torchvision.transforms.ToTensor(),
                               torchvision.transforms.Normalize(
                                 (0.1307,), (0.3081,))
                             ])),
  batch_size=batch_size, shuffle=True)


class nn714(nn.Module):

    def __init__(self):

        super(nn714, self).__init__()

        self.conv1 = nn.Conv2d(1, 32, kernel_size=3, stride=2)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=3, stride=2)
        self.fc1 = nn.Linear(2304, 256)
        self.fc2 = nn.Linear(256, 47)

    def forward(self, x):

        x = x.view(-1, 1, 28, 28)
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = x.view(-1, 2304)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)

        return x


nn714 = nn714().to(device)

criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(nn714.parameters(), lr=learning_rate)

loss_list = []
acc_list = []

for epoch in range(max_iters):  # loop over the dataset multiple times

    total_loss = 0.
    avg_acc = 0.
    for batch_idx, (data, target) in enumerate(train_loader):

        gt = target.data.numpy()

        # zero the parameter gradients
        optimizer.zero_grad()
        # forward + backward + optimize
        outputs = nn714(data.to(device))

        pred = np.argmax(outputs.cpu().data.numpy(), axis=1)
        correct = np.equal(gt, pred).astype(np.float32)
        acc = np.sum(correct) / correct.shape[0]

        loss = criterion(outputs, target.to(device))
        loss.backward()
        optimizer.step()

        # print statistics
        avg_acc += acc
        total_loss += loss.item()

    avg_acc /= (batch_idx+1)
    avg_loss = total_loss / (batch_idx+1)
    loss_list.append(avg_loss)
    acc_list.append(avg_acc)

    print('[Epoch %d] Training Loss: %.3f, Training Acc: %.3f' % (epoch + 1, avg_loss, avg_acc))
    total_loss = 0.0
    avg_acc = 0.0

print('Finished Training')
torch.save(nn714.state_dict(), './weights.pth')

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(np.arange(max_iters), acc_list, 'r-')
fig.show()

fig2 = plt.figure()
ax2 = fig2.add_subplot(111)
ax2.plot(np.arange(max_iters), loss_list, 'r-')
fig2.show()

plt.show()

print('Starting Evaluation')
nn714.load_state_dict(torch.load('weights.pth'))

for img in os.listdir('../images'):
    im1 = skimage.img_as_float(skimage.io.imread(os.path.join('../images',img)))
    bboxes, bw = findLetters(im1)

    plt.imshow(bw)
    for bbox in bboxes:
        minr, minc, maxr, maxc = bbox
        rect = matplotlib.patches.Rectangle((minc, minr), maxc - minc, maxr - minr,
                                fill=False, edgecolor='red', linewidth=2)
        plt.gca().add_patch(rect)
    plt.show()
    # find the rows using..RANSAC, counting, clustering, etc.
    r_idxs = []
    c_idxs = []
    idx = []
    max_length = 0.
    for i, bbox in enumerate(bboxes):
        minr, minc, maxr, maxc = bbox
        midr = minr + (maxr - minr) / 2.
        midc = minc + (maxc - minc) / 2.
        r_idxs.append(midr)
        c_idxs.append(midc)
    count_r_idxs = [0]
    idx = 0
    newline = []
    for i in range(1, len(r_idxs), 1):
        if np.abs(r_idxs[i] - r_idxs[i-1]) > 80:
            idx += 1
            newline.append(i-1)
        count_r_idxs.append(idx)
    r_idxs = np.stack(count_r_idxs)
    c_idxs = np.stack(c_idxs)
    order_idxs = np.lexsort((c_idxs, r_idxs))
    # crop the bounding boxes
    crops = []
    for idx in order_idxs:
        minr, minc, maxr, maxc = bboxes[idx]
        crop = bw[minr:maxr, minc:maxc]
        diff = (maxc - minc) - (maxr - minr)
        if diff > 0:
            crop = np.pad(crop, ((diff//2, diff//2), (0, 0)), 'constant', constant_values=1.0)
        else:
            crop = np.pad(crop, ((0, 0), (-diff//2, -diff//2)), 'constant', constant_values=1.0)
        crop = np.invert(skimage.morphology.binary_erosion(crop))
        crop = skimage.morphology.dilation(crop).astype(np.float)
        crop = np.pad(crop, ((30, 30), (30, 30)), 'constant', constant_values=0.0)
        resized_crop = skimage.transform.resize(crop, (28, 28)).T
        #skimage.io.imshow(resized_crop)
        #skimage.io.show()
        crops.append(resized_crop)
    # note.. before you flatten, transpose the image (that's how the dataset is!)
    # consider doing a square crop, and even using np.pad() to get your images looking more like the dataset
    
    # load the weights
    # run the crops through your neural network and print them out
    for i, crop in enumerate(crops):
        data = Variable(torch.from_numpy(crop)).float()
        outputs = nn714(data.to(device))
        pred_idx = np.squeeze(np.argmax(outputs.cpu().data.numpy(), axis=1))
        # What do the idx correspond to?
        if pred_idx < 10:
            print (pred_idx, end='')
        elif pred_idx < 36 and pred_idx >= 10:
            print (chr(pred_idx-10+65), end='')
        elif pred_idx == 37:
            print (chr(98), end='')
        elif pred_idx == 38:
            print (chr(99), end='')
        else:
            print (chr(pred_idx-38+100), end='')               
        if i in newline:
            print ('')
    print ('\n')