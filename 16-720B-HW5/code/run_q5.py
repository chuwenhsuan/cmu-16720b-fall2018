import numpy as np
import scipy.io
from nn import *
from collections import Counter

train_data = scipy.io.loadmat('../data/nist36_train.mat')
valid_data = scipy.io.loadmat('../data/nist36_valid.mat')

# we don't need labels now!
train_x = train_data['train_data']
valid_x = valid_data['valid_data']
valid_y = valid_data['valid_labels']

CLASS_IDX = [0, 3, 14, 17, 29]

max_iters = 100
# pick a batch size, learning rate
batch_size = 36 
learning_rate =  3e-5
hidden_size = 32
lr_rate = 20
batches = get_random_batches(train_x,np.ones((train_x.shape[0],1)),batch_size)
batch_num = len(batches)

params = Counter()

# initialize layers here

initialize_weights(1024,hidden_size,params,'layer1')
initialize_weights(hidden_size,hidden_size,params,'hidden')
initialize_weights(hidden_size,hidden_size,params,'hidden2')
initialize_weights(hidden_size,1024,params,'output')

params["m_W" + "layer1"] = np.zeros(params['W' + "layer1"].shape)
params["m_b" + "layer1"] = np.zeros(params['b' + "layer1"].shape)
params["m_W" + "hidden"] = np.zeros(params['W' + "hidden"].shape)
params["m_b" + "hidden"] = np.zeros(params['b' + "hidden"].shape)
params["m_W" + "hidden2"] = np.zeros(params['W' + "hidden2"].shape)
params["m_b" + "hidden2"] = np.zeros(params['b' + "hidden2"].shape)
params["m_W" + "output"] = np.zeros(params['W' + "output"].shape)
params["m_b" + "output"] = np.zeros(params['b' + "output"].shape)

# should look like your previous training loops

train_loss_list = []

for itr in range(max_iters):
    total_loss = 0
    for xb,_ in batches:

        # training loop can be exactly the same as q2!
        # your loss is now squared error
        # delta is the d/dx of (x-y)^2
        # to implement momentum
        #   just use 'm_'+name variables
        #   to keep a saved value over timestamps
        #   params is a Counter(), which returns a 0 if an element is missing
        #   so you should be able to write your loop without any special conditions

        post_act1 = forward(xb, params, "layer1", activation=relu)
        post_act2 = forward(post_act1, params, "hidden", activation=relu)
        post_act3 = forward(post_act2, params, "hidden2", activation=relu)
        pred = forward(post_act3, params, "output")

        loss = np.sum((pred - xb)**2)
        total_loss += loss / len(xb)

        delta1 = 0.5 * (pred - xb)
        delta2 = backwards(delta1, params, "output", sigmoid_deriv)
        delta3 = backwards(delta2, params, "hidden2", relu_deriv)
        delta4 = backwards(delta3, params, "hidden", relu_deriv)
        _ = backwards(delta4, params, "layer1", relu_deriv)

        params['m_W' + "layer1"] = 0.9 * params['m_W' + "layer1"] - learning_rate * params['grad_W' + "layer1"]
        params['m_b' + "layer1"] = 0.9 * params['m_b' + "layer1"] - learning_rate * params['grad_b' + "layer1"]
        params['m_W' + "hidden"] = 0.9 * params['m_W' + "hidden"] - learning_rate * params['grad_W' + "hidden"]
        params['m_b' + "hidden"] = 0.9 * params['m_b' + "hidden"] - learning_rate * params['grad_b' + "hidden"]
        params['m_W' + "hidden2"] = 0.9 * params['m_W' + "hidden2"] - learning_rate * params['grad_W' + "hidden2"]
        params['m_b' + "hidden2"] = 0.9 * params['m_b' + "hidden2"] - learning_rate * params['grad_b' + "hidden2"]
        params['m_W' + "output"] = 0.9 * params['m_W' + "output"] - learning_rate * params['grad_W' + "output"]
        params['m_b' + "output"] = 0.9 * params['m_b' + "output"] - learning_rate * params['grad_b' + "output"]

        params['W' + "layer1"] += params['m_W' + "layer1"]
        params['b' + "layer1"] += params['m_b' + "layer1"]
        params['W' + "hidden"] += params['m_W' + "hidden"]
        params['b' + "hidden"] += params['m_b' + "hidden"]
        params['W' + "hidden2"] += params['m_W' + "hidden2"]
        params['b' + "hidden2"] += params['m_b' + "hidden2"]
        params['W' + "output"] += params['m_W' + "output"]
        params['b' + "output"] += params['m_b' + "output"]

    avg_loss = total_loss / len(batches)
    train_loss_list.append(avg_loss)
    if itr % 2 == 0:
        print("itr: {:02d} \t loss: {:.2f}".format(itr,avg_loss))
    if itr % lr_rate == lr_rate-1:
        learning_rate *= 0.9

train_loss_list = np.stack(train_loss_list)

# visualize some results
# Q5.3.1
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(np.arange(max_iters), train_loss_list, 'r-')
fig.show()
plt.show()

count = 0
for i in range(len(valid_x)):

    if len(CLASS_IDX) == 0: break

    if count >= 2:
        del CLASS_IDX[0]
        count = 0

    if np.argmax(valid_y[i]) in CLASS_IDX:

        h1 = forward(valid_x[i],params,'layer1',relu)
        h2 = forward(h1,params,'hidden',relu)
        h3 = forward(h2,params,'hidden2',relu)
        recon = forward(h3,params,'output',sigmoid)

        plt.subplot(2,1,1)
        plt.imshow(valid_x[i].reshape(32,32).T)
        plt.subplot(2,1,2)
        plt.imshow(recon.reshape(32,32).T)
        plt.show()

        count += 1


from skimage.measure import compare_psnr as psnr
# evaluate PSNR
# Q5.3.2
h1 = forward(valid_x,params,'layer1',relu)
h2 = forward(h1,params,'hidden',relu)
h3 = forward(h2,params,'hidden2',relu)
out = forward(h3,params,'output',sigmoid)

total_psnr = psnr(valid_x, out)
print (total_psnr)