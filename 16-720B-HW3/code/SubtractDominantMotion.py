import numpy as np
import scipy.ndimage
from scipy.interpolate import RectBivariateSpline
from LucasKanadeAffine import LucasKanadeAffine
from InverseCompositionAffine import InverseCompositionAffine

def SubtractDominantMotion(image1, image2):
    # Input:
    #	Images at time t and t+1 
    # Output:
    #	mask: [nxm]
    # put your implementation here

    CONST_HOMO_ROW = np.array([[0.0, 0.0, 1.0]], dtype=np.float32)
    CONST_ONES = np.ones(image1.shape, dtype=np.float32)

    X, Y = np.meshgrid(np.arange(image2.shape[1]), np.arange(image2.shape[0]))
    X = np.ndarray.flatten(X)
    Y = np.ndarray.flatten(Y)
    xy1 = np.stack([X, Y, np.ones(X.shape)], axis=0)

    interpolator_image2 = RectBivariateSpline(np.arange(image2.shape[0]), np.arange(image2.shape[1]), image2)
    interpolator_mask = RectBivariateSpline(np.arange(CONST_ONES.shape[0]), np.arange(CONST_ONES.shape[1]), CONST_ONES)

    #M = LucasKanadeAffine(image1, image2)
    M = InverseCompositionAffine(image1, image2)
    M_homo = np.append(M, CONST_HOMO_ROW, axis=0)
    warped_xy1 = M_homo @ xy1
    warped_xy1 = warped_xy1 / warped_xy1[-1]
    image2_warped = interpolator_image2.ev(warped_xy1[1,:], warped_xy1[0, :]).reshape(image2.shape)
    exist_mask = np.sign(interpolator_mask.ev(warped_xy1[1,:], warped_xy1[0, :]).reshape(CONST_ONES.shape))
    img_diff = np.absolute(image2_warped - image1) * exist_mask

    threshold_mask = np.greater(img_diff, 0.05)
    mask = scipy.ndimage.morphology.binary_dilation(scipy.ndimage.morphology.binary_erosion(threshold_mask, iterations=1), iterations=1)
    mask = scipy.ndimage.morphology.binary_dilation(mask, iterations=1)

    return mask
