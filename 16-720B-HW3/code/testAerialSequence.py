import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib.patches as patches
from SubtractDominantMotion import SubtractDominantMotion
import cv2

# write your script here, we recommend the above libraries for making your animation
if __name__ == '__main__':
    
    aerial_sequence = np.load("../data/aerialseq.npy")
    frames = [30, 60, 90, 120]

    CONST_BLUE = np.zeros(aerial_sequence.shape[0:2] + (3, ))
    CONST_BLUE[:] = (1.0, 0.0, 0.0)

    for i in frames:

        mask = SubtractDominantMotion(aerial_sequence[:,:,i], aerial_sequence[:,:,i+1])
        mask = CONST_BLUE * np.tile(np.expand_dims(mask, -1), (1, 1, 3))

        img = aerial_sequence[:,:,i].copy()
        img = np.tile(np.expand_dims(img, -1), (1, 1, 3))
        cv2.imwrite("./aerial_seq_frame_{}.jpg".format(str(i)), np.maximum(mask, img)*255.0)

