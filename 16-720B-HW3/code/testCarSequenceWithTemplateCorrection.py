import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib.patches as patches
from LucasKanade import LucasKanade
import cv2

# write your script here, we recommend the above libraries for making your animation
if __name__ == '__main__':

    car_sequence = np.load("../data/carseq.npy")

    p = (0, 0)
    frames = [1, 100, 200, 300, 400]
    rect0 = [59, 116, 145, 151]
    rect_naive = [59, 116, 145, 151]
    rect = [59, 116, 145, 151]
    rect_list = []

    for i in range(car_sequence.shape[-1]-1):

        # For comparison
        p_naive = LucasKanade(car_sequence[:,:,i], car_sequence[:,:,i+1], np.array(rect_naive, dtype=np.int))
        rect_naive = [rect_naive[0]+p_naive[0], rect_naive[1]+p_naive[1], rect_naive[2]+p_naive[0], rect_naive[3]+p_naive[1]]

        p = LucasKanade(car_sequence[:,:,i], car_sequence[:,:,i+1], np.array(rect, dtype=np.int))
        rect_temp = [rect[0]+p[0], rect[1]+p[1], rect[2]+p[0], rect[3]+p[1]]
        p_sum = np.array([rect_temp[0]-rect0[0], rect_temp[1]-rect0[1]], dtype=np.float32)
        p_star = LucasKanade(car_sequence[:,:,0], car_sequence[:,:,i+1], np.array(rect0, dtype=np.int), p_sum)
        if np.linalg.norm(p_star-p_sum) < 3:
            rect = [rect0[0]+p_star[0], rect0[1]+p_star[1], rect0[2]+p_star[0], rect0[3]+p_star[1]]
        else:
            rect = rect_temp
        rect_list.append(np.array(rect, dtype=np.float32))

        # Visualization
        if i in frames:

            img = car_sequence[:,:,i].copy()
            img = np.tile(np.expand_dims(img, -1), (1, 1, 3))
            rect_naive_int = np.array(rect_naive, dtype=np.int)
            rect_int = np.array(rect, dtype=np.int)
            cv2.rectangle(img, (rect_naive_int[0], rect_naive_int[1]), (rect_naive_int[2], rect_naive_int[3]), (255, 0, 0)) # For comparison
            cv2.rectangle(img, (rect_int[0], rect_int[1]), (rect_int[2], rect_int[3]), (0, 0, 255))
            cv2.imwrite("./car_seq_wcrt_frame_{}.jpg".format(str(i)), img*255.0)
    
    np.save("./carseqrects-wcrt.npy", np.stack(rect_list, axis=0))