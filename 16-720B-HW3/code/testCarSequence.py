import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib.patches as patches
from LucasKanade import LucasKanade
import cv2

# write your script here, we recommend the above libraries for making your animation
if __name__ == '__main__':

    car_sequence = np.load("../data/carseq.npy")
    
    p = (0, 0)
    frames = [1, 100, 200, 300, 400]
    rect = [59, 116, 145, 151]
    rect_list = []

    for i in range(car_sequence.shape[-1]-1):

        p = LucasKanade(car_sequence[:,:,i], car_sequence[:,:,i+1], np.array(rect, dtype=np.int))
        rect = [rect[0]+p[0], rect[1]+p[1], rect[2]+p[0], rect[3]+p[1]]
        rect_list.append(np.array(rect, dtype=np.float32))
    
        # Visualization
        if i in frames:

            img = car_sequence[:,:,i].copy()
            img = np.tile(np.expand_dims(img, -1), (1, 1, 3))
            rect_int = np.array(rect, dtype=np.int)
            cv2.rectangle(img, (rect_int[0], rect_int[1]), (rect_int[2], rect_int[3]), (255, 0, 0))
            cv2.imwrite("./car_seq_frame_{}.jpg".format(str(i)), img*255.0)
    
    np.save("./carseqrects.npy", np.stack(rect_list, axis=0))