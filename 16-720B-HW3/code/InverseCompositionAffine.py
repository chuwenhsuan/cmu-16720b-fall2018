import numpy as np
import scipy.signal
from scipy.interpolate import RectBivariateSpline

def InverseCompositionAffine(It, It1):
    # Input: 
    #	It: template image
    #	It1: Current image
    # Output:
    #	M: the Affine warp matrix [2x3 numpy array]
    # put your implementation here
    
    CONST_HOMO_ROW = np.array([[0.0, 0.0, 1.0]], dtype=np.float32)
    CONST_ONES = np.ones(It1.shape, dtype=np.float32)

    p = np.zeros(6, dtype=np.float32)

    update_size = 10.0

    T_grad_x = np.ndarray.flatten(np.gradient(It, axis=1))
    T_grad_y = np.ndarray.flatten(np.gradient(It, axis=0))

    X, Y = np.meshgrid(np.arange(It1.shape[1]), np.arange(It1.shape[0]))
    X = np.ndarray.flatten(X)
    Y = np.ndarray.flatten(Y)
    xy1 = np.stack([X, Y, np.ones(X.shape)], axis=0)

    interpolator_It1 = RectBivariateSpline(np.arange(It1.shape[0]), np.arange(It1.shape[1]), It1)
    interpolator_mask = RectBivariateSpline(np.arange(CONST_ONES.shape[0]), np.arange(CONST_ONES.shape[1]), CONST_ONES)

    x_vec = np.tile(np.arange(It.shape[1], dtype=np.float32), It.shape[0])
    y_vec = np.repeat(np.arange(It1.shape[0], dtype=np.float32), It1.shape[1])
    sd_imgs = np.array([x_vec*T_grad_x, y_vec*T_grad_x, T_grad_x, x_vec*T_grad_y, y_vec*T_grad_y, T_grad_y], dtype=np.float32)
    hessian = sd_imgs @ np.transpose(sd_imgs)
    hessian_inv = np.linalg.pinv(hessian)

    M = np.array([[1.0+p[0], p[1], p[2]], [p[3], 1.0+p[4], p[5]]], dtype=np.float32)

    while update_size > 0.01:

        M_homo = np.append(M, CONST_HOMO_ROW, axis=0)
        warped_xy1 = M_homo @ xy1
        warped_xy1 = warped_xy1 / warped_xy1[-1]

        I_warped = interpolator_It1.ev(warped_xy1[1,:], warped_xy1[0, :]).reshape(It1.shape)
        mask = np.sign(interpolator_mask.ev(warped_xy1[1,:], warped_xy1[0, :]).reshape(CONST_ONES.shape))

        patch_diff = np.ndarray.flatten(mask * (I_warped - It))
        sd_update = sd_imgs @ patch_diff
        delta_p = hessian_inv.dot(sd_update)
        M_new = np.array([[1.0+delta_p[0], delta_p[1], delta_p[2]], [delta_p[3], 1.0+delta_p[4], delta_p[5]]], dtype=np.float32)
        M_new_homo = np.append(M_new, CONST_HOMO_ROW, axis=0)
        M_updated_homo = M_homo @ np.linalg.inv(M_new_homo)
        M_updated_homo = M_updated_homo / M_updated_homo[-1,-1]
        M = M_updated_homo[0:2, :]

        update_size = np.linalg.norm(delta_p)

    return M