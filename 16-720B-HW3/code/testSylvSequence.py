import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib.patches as patches
from LucasKanade import LucasKanade
from LucasKanadeBasis import LucasKanadeBasis
import cv2

# write your script here, we recommend the above libraries for making your animation
if __name__ == '__main__':

    sylv_bases = np.load("../data/sylvbases.npy")
    sylv_sequence = np.load("../data/sylvseq.npy")

    p = (0, 0)
    frames = [1, 200, 300, 350, 400]
    rect = [101, 61, 155, 107]
    rect_naive = [101, 61, 155, 107]
    rect_list = []

    for i in range(sylv_sequence.shape[-1]-1):

        # For comparison
        p_naive = LucasKanade(sylv_sequence[:,:,i], sylv_sequence[:,:,i+1], np.array(rect_naive, dtype=np.int))
        rect_naive = [rect_naive[0]+p_naive[0], rect_naive[1]+p_naive[1], rect_naive[2]+p_naive[0], rect_naive[3]+p_naive[1]]

        p = LucasKanadeBasis(sylv_sequence[:,:,i], sylv_sequence[:,:,i+1], np.array(rect, dtype=np.int), sylv_bases)
        rect = [rect[0]+p[0], rect[1]+p[1], rect[2]+p[0], rect[3]+p[1]]
        rect_list.append(np.array(rect, dtype=np.float32))
    
        if i in frames:

            img = sylv_sequence[:,:,i].copy()
            img = np.tile(np.expand_dims(img, -1), (1, 1, 3))
            rect_naive_int = np.array(rect_naive, dtype=np.int)
            rect_int = np.array(rect, dtype=np.int)
            cv2.rectangle(img, (rect_naive_int[0], rect_naive_int[1]), (rect_naive_int[2], rect_naive_int[3]), (255, 0, 0)) # For comparison
            cv2.rectangle(img, (rect_int[0], rect_int[1]), (rect_int[2], rect_int[3]), (0, 0, 255))
            cv2.imwrite("./sylv_seq_frame_{}.jpg".format(str(i)), img*255.0)
    
    np.save("./sylvseqrects.npy", np.stack(rect_list, axis=0))