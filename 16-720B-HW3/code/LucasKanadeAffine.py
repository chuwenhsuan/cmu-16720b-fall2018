import numpy as np
from scipy.interpolate import RectBivariateSpline

def LucasKanadeAffine(It, It1):
    # Input: 
    #	It: template image
    #	It1: Current image
    # Output:
    #	M: the Affine warp matrix [2x3 numpy array]
    # put your implementation here

    CONST_HOMO_ROW = np.array([[0.0, 0.0, 1.0]], dtype=np.float32)
    CONST_ONES = np.ones(It1.shape, dtype=np.float32)

    p = np.zeros(6, dtype=np.float32)

    update_size = 10.0

    I_grad_x = np.gradient(It1, axis=1)
    I_grad_y = np.gradient(It1, axis=0)

    X, Y = np.meshgrid(np.arange(It1.shape[1]), np.arange(It1.shape[0]))
    X = np.ndarray.flatten(X)
    Y = np.ndarray.flatten(Y)
    xy1 = np.stack([X, Y, np.ones(X.shape)], axis=0)

    interpolator_It1 = RectBivariateSpline(np.arange(It1.shape[0]), np.arange(It1.shape[1]), It1)
    interpolator_grad_x = RectBivariateSpline(np.arange(I_grad_x.shape[0]), np.arange(I_grad_x.shape[1]), I_grad_x)
    interpolator_grad_y = RectBivariateSpline(np.arange(I_grad_y.shape[0]), np.arange(I_grad_y.shape[1]), I_grad_y)
    interpolator_mask = RectBivariateSpline(np.arange(CONST_ONES.shape[0]), np.arange(CONST_ONES.shape[1]), CONST_ONES)

    x_vec = np.tile(np.arange(It1.shape[1], dtype=np.float32), It1.shape[0])
    y_vec = np.repeat(np.arange(It1.shape[0], dtype=np.float32), It1.shape[1])

    M = np.array([[1.0+p[0], p[1], p[2]], [p[3], 1.0+p[4], p[5]]], dtype=np.float32)

    while update_size > 0.01:

        M_homo = np.append(M, CONST_HOMO_ROW, axis=0)

        warped_xy1 = M_homo @ xy1
        warped_xy1 = warped_xy1 / warped_xy1[-1]

        I_warped = interpolator_It1.ev(warped_xy1[1,:], warped_xy1[0, :]).reshape(It1.shape)
        I_grad_x_warped = interpolator_grad_x.ev(warped_xy1[1,:], warped_xy1[0, :])
        I_grad_y_warped = interpolator_grad_y.ev(warped_xy1[1,:], warped_xy1[0, :])
        mask = np.sign(interpolator_mask.ev(warped_xy1[1,:], warped_xy1[0, :]).reshape(CONST_ONES.shape))

        patch_diff = np.ndarray.flatten(mask * (It - I_warped))
        sd_imgs = np.array([x_vec*I_grad_x_warped, y_vec*I_grad_x_warped, I_grad_x_warped, x_vec*I_grad_y_warped, y_vec*I_grad_y_warped, I_grad_y_warped], dtype=np.float32)
        hessian = sd_imgs @ np.transpose(sd_imgs)
        hessian_inv = np.linalg.pinv(hessian)
        sd_update = sd_imgs @ patch_diff
        delta_p = hessian_inv.dot(sd_update)
        p = p + delta_p
        M = np.array([[1.0+p[0], p[1], p[2]], [p[3], 1.0+p[4], p[5]]], dtype=np.float32)

        update_size = np.linalg.norm(delta_p)

    return M
