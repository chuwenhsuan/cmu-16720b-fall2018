import numpy as np
from scipy.interpolate import RectBivariateSpline

def LucasKanade(It, It1, rect, p0 = np.zeros(2)):
	# Input: 
	#	It: template image
	#	It1: Current image
	#	rect: Current position of the car
	#	(top left, bot right coordinates)
	#	p0: Initial movement vector [dp_x0, dp_y0]
	# Output:
	#	p: movement vector [dp_x, dp_y]
	
    # Put your implementation here

    p = p0

    update_size = 10.0

    top_left = rect[0:2]
    bot_right = rect[2:4]
    T_patch = It[top_left[1]:bot_right[1]+1, top_left[0]:bot_right[0]+1]

    I_grad_x = np.gradient(It1, axis=1)
    I_grad_y = np.gradient(It1, axis=0)

    X, Y = np.meshgrid(np.arange(It1.shape[1]), np.arange(It1.shape[0]))
    X = np.ndarray.flatten(X)
    Y = np.ndarray.flatten(Y)
    xy1 = np.stack([X, Y, np.ones(X.shape)], axis=0)

    interpolator_It1 = RectBivariateSpline(np.arange(It1.shape[0]), np.arange(It1.shape[1]), It1)
    interpolator_grad_x = RectBivariateSpline(np.arange(I_grad_x.shape[0]), np.arange(I_grad_x.shape[1]), I_grad_x)
    interpolator_grad_y = RectBivariateSpline(np.arange(I_grad_y.shape[0]), np.arange(I_grad_y.shape[1]), I_grad_y)

    M_homo = np.array([[1.0, 0.0, p[0]], [0.0, 1.0, p[1]], [0.0, 0.0, 1.0]], dtype=np.float32)

    while update_size > 0.01:

        warped_xy1 = M_homo @ xy1
        warped_xy1 = warped_xy1 / warped_xy1[-1]

        I_warped = interpolator_It1.ev(warped_xy1[1,:], warped_xy1[0, :]).reshape(It1.shape)
        I_grad_x_warped = interpolator_grad_x.ev(warped_xy1[1,:], warped_xy1[0, :]).reshape(I_grad_x.shape)
        I_grad_y_warped = interpolator_grad_y.ev(warped_xy1[1,:], warped_xy1[0, :]).reshape(I_grad_y.shape)
        I_patch = I_warped[top_left[1]:bot_right[1]+1, top_left[0]:bot_right[0]+1]
        I_grad_x_warped_patch = np.ndarray.flatten(I_grad_x_warped[top_left[1]:bot_right[1]+1, top_left[0]:bot_right[0]+1])
        I_grad_y_warped_patch = np.ndarray.flatten(I_grad_y_warped[top_left[1]:bot_right[1]+1, top_left[0]:bot_right[0]+1])

        patch_diff = np.ndarray.flatten(T_patch - I_patch)
        sd_imgs = np.array([I_grad_x_warped_patch, I_grad_y_warped_patch], dtype=np.float32)
        hessian = sd_imgs @ np.transpose(sd_imgs)
        hessian_inv = np.linalg.pinv(hessian)
        sd_update = sd_imgs @ patch_diff
        delta_p = hessian_inv.dot(sd_update)
        p = p + delta_p

        M_homo = np.array([[1.0, 0.0, p[0]], [0.0, 1.0, p[1]], [0.0, 0.0, 1.0]], dtype=np.float32)

        update_size = np.linalg.norm(delta_p)

    return p
